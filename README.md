# Lore

The fictional misadventures of the LHWHS Computer Science Club. Check out [pilogy](pilogy.pdf)!

## Expand our universe!

Love writing or worldbuilding? Help out by expanding our collection of stories! We appreciate all contributions!
