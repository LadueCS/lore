#!/usr/bin/bash
# paru -S pandoc python-weasyprint pdftk --asdeps

# Creat HTML
pandoc title.md zero.md one.md two.md pi.md note.md blurb.md -c style.css -s --metadata title=pilogy -o pilogy.html

# Create PDF
pandoc pilogy.html -c style.css --metadata title="" --pdf-engine=weasyprint -o tmp.pdf

# pdftk preserves the table of contents
# but somehow mangles "Mr. FFFFFF" for some reason
pdftk img/cover.pdf tmp.pdf cat output pilogy.pdf

rm tmp.pdf
