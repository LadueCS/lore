#!/usr/bin/bash

montage alpaca.png arch.png beaver.png breadco.png \
        biang.png tesla.png timezones.png bsod.png \
        equation.png dogecoin.png fox.png froyo.png \
        mao.png kazakhstan.png gerrit.png linux.png \
        mastodon.png laduecs.png pallas.png guido.png \
        apl.png smiley.png azerbaijan.png squirrel.png \
        -tile 4x -geometry "720x720>+0+0" background.png
