## note

When I was a little kid I hated writing.

I'd rather not talk about it, since it would probably give me traumatic flashbacks, but I think having a breakdown in a writing class is a universal human experience governed under the Geneva Convention.

Reading, however, was a different story, pun totally intended.

I have an ancient photo of one of my middle school friends standing under a wall that says:

```
Books are
      por
```

For months, Isabella and I wildly speculated on what the part of the wall not in the photo said. Finally, I resolved this for once and for all by geoguessing where the photo was taken, and visiting that location. It actually says:

```
Books are a uniquely
      portable magic
      - Stephen King
```

Yes! So true!

There are some books that have completely changed my life. Well, maybe all books have because of the butterfly effect. Books like *What If?*, *The Elements*, *Origami and Math*, *Gödel, Escher, Bach*, and *The Hitchhiker's Guide to the Galaxy*. Wait, that last one sounds familiar!

*The Hitchhiker's Guide to Arch Linux* was part of a long series of blog posts that I wrote in 2021 and 2022 for an experiment to desensitize me to the pain and torment of writing. Actually, it turns out writing isn't so bad when you're writing a blog post about evil Linux commands that will totally destroy your computer. After a few of those posts, I started dabbling in fiction too, hence *The Hitchhiker's Guide to Arch Linux*, which eventually became book zero in *Pilogy*.

Also, in case you didn't realize it, *Pilogy* is a work of fiction. Any resemblance to actual events or locales or persons, living or dead, is not coincidental and you should sue us. The characters are based off of those in [Intro to Python](https://codeberg.org/LadueCS/Intro-to-Python). Please do yourself a favor and learn Python through our wonderful course! Hooray, shameless self-promotion!

Expanding *The Hitchhiker's Guide to Arch Linux* to a full book soon became one of my many half-finished and abandoned projects.

Then, ChatGPT happened.

I have a lot of weird thoughts. And ChatGPT has a lot of weird answers. For one, it's surprisingly good at writing [satirical news articles](https://a.exozy.me/posts/ainion/).

Over the 2022.5 winter break, Isabella forced me to watch the TV show *Avatar: The Last Airbender* with her. I've never actually seen it before, perhaps because it's the same age as me, but Isabella has somehow seen it a million times already and spoiled enough stuff to keep me annoyed but not too annoyed, such as Aang growing hair. Naturally, I fed ChatGPT some *Avatar*-related prompts like shipping hurricanes and Avatar characters, but the most hilarious output I got was from asking it to write a screenplay for an episode where one of the characters is an anachronistic cryptobro. (Hey, maybe they have analog human-powered cryptocurrencies and hash functions you can compute on pencil-and-paper)

Isabella and I (mostly Isabella) compiled and edited the best outputs into a short book called *Avatar* (named to cause maximum confusion because there are a million franchises with that name). Isabella also stuffed it with a bunch of jokes and gags, since ChatGPT's jokes are usually garbage. She's a much better writer than I am, and also livened up the sometimes boring writing process. That became book one in *Pilogy*.

After *Avatar*, the project went straight back into my half-finished heap of abandonment, but I still played around with ChatGPT sometimes. I made a file called `ideas.md` where I jotted down weird ideas whenever they popped into my head, building up a potpourri of truly bizarre ChatGPT prompts. Prompts about military-grade beaver urinals, global socialist daycare conspiracies, the epistemology of Azerbaijan, and shattering objects by whistling at their resonant frequency resulted in *Beavers*, *Daymare*, *Azerbaijan*, and *Whistle* respectively, some of which I published on my website. I got tired of writing about the Wenders++, so none of these stories were about them and I didn't really know how to integrate them into the Intro to Python universe.

The solution? Get meta.

The second and pi-th books are actually the literary equivalent of glue code to bind together some experimental writing and surreal short stories while providing an overarching narrative of an epic battle between the Wenders++ and Ezon. I spent at least a month brainstorming a satisfying and unique climax and ending for the book. Finally, I came up with a *Monty Python and the Holy Grail* inspired ending that is neither satisfying nor unique, but hey, no one expects it!

I'd say that around 30% of the book is written by ChatGPT, mainly the boring parts, because that's what it's good at writing. Also, it was immensely useful for prototyping. Of course, it can't replicate our sense of humor at all, so all the jokes are human-written. And the prompts too. ChatGPT would never willingly talk about military-grade beaver urinals if we didn't think of that bright idea.

Anyways, I hope you had as much fun reading this as I had!

This book was created with Pandoc, WeasyPrint, the Source Code Pro font, Noto fonts, Krita (the NOR filter is insane!), (a ridiculous amount of) ImageMagick, the Press Start 2P font, librsvg, Waifu2x (very important!), and PDFtk. Thanks to all the developers out there who built these amazing tools! You can browse all the raw Markdown files, images, and atrocious shell scripts for creating this book on the [LadueCS Codeberg](https://codeberg.org/LadueCS/Lore). This book is licensed under CC BY-SA 4.0, which in normal-person-speak, means you can pirate and remix this book all you want as long as you publish your masterpiece under the same license.
