## pi

The entire planet of Earth has a problem, which is that its computers enjoy malfunctioning and watching you cry. Many solutions have been suggested for this problem, but most of them involved creating more and more powerful and smart computers that only gain more and more satisfaction from watching you cry.

And so the problem remains, and people only cried more and more from their computers malfunctioning. Some people thought that the problem was the computers themselves, and that computers never should have been invented and we should just switch to using paper and pencil for surveillance capitalism, but they were quickly apprehended by the Facebook Bureau of Investigation and permabanned from the internet. Some were even permabanned from IRL.

And then, one Out of Touch Thursday evening, Lalani was coding at a small little café, since that's precisely what cafés are meant for, and suddenly an arc lamp literally went off on in her head figuratively and she realized what was going wrong with the world all this time, and she finally knew how to make humans and computers actually get along together.

Sadly, before she her laptop could allocate enough RAM for her bloated Electron chat app so she could message LHWHSCSC about her idea, a terribly stupid catastrophe happened, and the idea was lost forever.

This is her story.

But first...

"Who are you, and why are you in my house?!?!"

Billiam Wender is standing confused in the basement of a bathroom in an average suburban house at 123 Example Street, watching a 30-year-old man watch anime using VLC media player plopped on a rusty blue bed.

"Moses! Don't be rude to our guest like that! Say sorry to him!" his elderly mother reprimands him as she asks Billiam if he wants a Capri Sun.

"No thanks, I don't like Capri Suns," Billiam says hastily.

"If you're wondering why our house's bathroom has a basement, the previous owners were quite colorful characters," Moses's mother explains, "They hid weapons in a secret chamber under the bathroom. Now my son uses it as his bedroom."

"So, um, I'm Billiam Wender," Billiam says, "And you are... Moses? Wait, as in... Moses Schmindler?"

"I'm not saying sorry to some random kid with such poor taste in juice," Moses replies angrily, "And yes that's my name. Why do you even care? What do you even want? Stop bothering me and let me focus on this anime movie. It's called *Daymare* and it won a bunch of awards."

On the TV, four high school anime girls are riding public transportation and chatting excitedly about... anime, their favorite obsession.

"You're..." Billiam pauses in awe, "Are you the mythical legend who founded of LHWHSCSC?"

"Hey, have you all seen the latest episode of *Akame ga Kiru!*? It was so good!" chatters Rei, one of the girls in the anime.

"Mythical? Are you high? If you're not doing juice, what are you on?" Moses yells, "There's nothing mythical about it. I'm not a hero. I just founded the club, that's it."

"Mr. FFFFFF likes to blow it up like it's a myth. He talks about you like--"

"Mr. FFFFFF..." Moses grumbles, his eyes maliciously glued to the TV, "Now that's a name I haven't heard in a while."

"*Akame ga Kiru!* SUcks!" rants Tsuki,  another character in the show, "It's sooooo overdramATic and self-CONSOLidatING. What's WITH this ANti-feminist POTrayal of strong WOMen? It's SO offensive and DIStasteful to watch! And WHY are the romance SCENes so CHEESY and STAGY? It's just NOT believABLE. Do the WRITers seriously THINK that peoPLE are goING to be inTIMidated by the STUPID violence and BLOOD? It's just so DRILLSylistic and ABSurD. TO top it ALL off, the CHARacters are hOPELessly one DimenSIONal and CARTOonISH."

"OK, we get it, calm down," Rei says, annoyed.

"Wait, why does the bus driver keep on vomiting?" asks Mirai, the third protagonist, "Every time he throws up, the bus spins out of control and that makes him more and more nauseous!"

"This is hiLARious," Tsuki giggles, "Now if THIS was an ANIme, I woULD toTALly watch IT!"

"Yeah, let's just hope we don't end up in a real-life *Death Note* scenario," the fourth girl replies in a monotone voice.

"That doesn't even make any sense!" screams Moses, "Why'd the movie writers have to nerf [To Be Filled By O.E.M.] like that? She has like, zero brain cells now."

"Huh, O.E.M. what?" Billiam asks.

"Her name is left bracket To Be Filled By O dot E dot M dot right bracket," Moses corrects him hurriedly, "Get it right, kid. That's her name. Don't tease her about it. That's not very nice."

Suddenly, the bus driver starts convulsing like a foldable phone and retches all over the steering wheel. The bus swerves wildly, causing the passengers to scream in terror.

"Oh my gosh, oh my gosh, what do we do?!" Rei panics.

"I know, LET's hijACK the bus!" Tsuki snickers.

"Yes! Let's do it!" Mirai replies hyperactively and enthusiastically.

"This is gonna sound crazy," Billiam, "But I suspect Ezon Muzk is trying to kidnap me and my siblings."

"Ezon Muzk?" Moses suddenly asks, shocked, turning his head to look at Billiam for the first time, "Kid, listen to me. You gotta stop doing drugs. You're obviously high."

The girls all look at each other and nod in agreement. They quickly overpower the driver and look at each other again awkwardly.

"Okay, now what?" Rei asks nervously.

"NOW? We DRIVE!" Tsuki cackles.

"I don't know how to drive!" Rei complains.

"Look, I don't know what I'm supposed to do to stop him," Billiam prods Moses again, but Moses just imagines Billiam doesn't exist.

"This is a great opportunity for you to learn how!" [To Be Filled By O.E.M.] adds, sarcastically.

"I'll give it a try, I guess..." Rei mutters.

The girls all cheer as Rei takes the driver's seat. However, it quickly becomes clear that she really has no idea how to drive a bus. The bus goes crazy, taking out several streetlights and a trash can full of Capri Sun pouches.

"Rei, what are you doing?!" asks Mirai, frantically.

"I don't know... all I'm learning is that driving a bus is nothing like Mario Kart!" replies Rei, energized but frustrated.

"Great. Now we're all going to die," [To Be Filled By O.E.M.] comments.

"Moses, you have to help me. My friends are going to die if you don't. Please!" Billiam begs.

"Not my problem. You're just gonna end up like that," Moses says, pointing at the smoldering wreckage of the bus on the TV.

The girls and the bus passengers quickly jump of the bus and survey the wreckage.

"Wow, that was actually so much fun!" Rei laughs, high on an adrenaline rush.

"Yeah, because nothing says fun like crashing a bus into a daycare," [To Be Filled By O.E.M.] says, facepalming.

The girls peak inside the gaping hole in the wall of a daycare created by the bus crash. They carefully crawl inside and find some kids napping and having daymares. Other kids are wide awake and screaming their lungs out.

"Now, now," says a friendly man as he walks over and hands a kid a Capri Sun, "Go to sleep, stop crying. Drink the JavaScript-flavored Capri Sun. Let the semicolons diffuse through your colon. You love syntactic sugar, don't you?"

"Great choice," remarks Moses.

"Ewww," Billiam comments.

"Who are you?" asks Rei, "And what the hell is going on here?"

"Shouldn't I be the one asking you that question, am I right?" the man smirks, "I'm a comptroller. I've been in charge of this place ever since the original owners were arrested for fraud. I'm the most honest man you'll ever meet."

"Um... why are all the kids crying?" Mirai asks, concerned.

"Oh, don't worry about it. Just a little experiment we're running."

"Yeah, because nothing says experiment like drugging innocent children with JavaScript-flavored Capri Suns," [To Be Filled By O.E.M.] yells out of nowhere, "What are you trying to do, create superhuman programmers?"

"Incorrect!" the comptroller exclaims, "We are actually a troika of devout followers of the one true ideology, Trotskyism, determined to overthrow the government and replace it with a socialist utopia!"

"What the hell?! That's insane!" yells Rei, shocked.

"Welcome to the real world, girls," the comptroller smirks, "You think this is just a daycare? No! This is a training ground for the next generation of socialist leaders!"

"OH, well THAT makes it ALL betTER THEN!" Tsuki jabs.

"Moses! You need to snap out of this anime bubble of yours!" Billiam shouts, "Welcome to the real world, Moses. My siblings need your help!"

"Stop bothering me!" Moses complains, "I'm trying to watch the movie!"

"We need to stop this. We can't let these kids be brainwashed like this," says Mirai, determined.

"Ha ha ha, you girls are so funny," the comptroller mocks them, "You think you can stop us? We have the power and resources to ensure our operation proceeds covertly until the day when we overthrow the government in flames!!"

"Great. Now we're stuck in a daycare run by socialist zealots scheming a government coup. I can't wait for what happens next!" [To Be Filled By O.E.M.] adds.

"Please, please, Moses, help me. Mr. FFFFFF told me your address and nothing else. I don't know what I'm supposed to do," Billiam pleads.

Just as the girls are about to confront the comptroller, the police arrive and arrest them for hijacking the bus.

"What, so Mr. FFFFFF suddenly blurted out my address and then, I don't know, died of a heart attack or something?" Moses says, annoyed.

"Yes! Well, kinda. He might still be alive. There were some gunshots, and now Ezon's niece is chasing me. It's a mess!" Billiam says.

"So you're telling me that you're a fugitive? In my house? And the police are gonna come to my house? What exactly do you want me to do about this?" asks Moses concerned, but in a selfish way.

"I don't know," Billiam replies, "Aren't you supposed to know?"

The girls are all jammed into the backseat of a police car, handcuffed.

"Why did we have to hijack that bus?" Rei groans, "Ugh, I can't think about it anymore, let's talk about something else. Anime. *A Certain Scientific Railgun* maybe."

"Please don't spoil it!" Mirai says, trying to ignore their plight, "I actually haven't started watching it yet."

"*A CerTain ScIenTific RAilgun* is the WORsT aNIme I'VE evER HAD the MISForTUNE of COMing aCROSS!" Tsuki begins to rant, "It's SO FULL of CLICHé ChARACtERS, ridiculously OPen-ENDED PLOTS, and ANNOYing SHOUtOUts to its SISter seRIES that I LOATHE evERY moMent I'M forced to WASte WATching it. It's got NOTHING on the truly LEGendARY anIME out there, like MY FAVORite *Squirrelvatar*! Heck, even BORing old Akame ga Kiru is INfiNITELY suPERIor to this CULtuRAL sewage. KARMA's noTHING more than a CARDBOard cutOuT, JUDGEMENT'S an utTER farce, and MISAKA's fRIENDship speEChES make me want to PUNCH the TV. This is the kind of SERIES that GIVES anime a bad NAME. All it does is ALLOW a GENeration of otAKUs to stoREly confRont a MEDiocre realIty and EXcuse themSELVes from progressING. *A Certain Scientific Railgun* NEEDS to be cancelled, NOW, and I wish its POtential would be BETter spent on ANother series which ACTually brings VALue to the anime SCene."

"Tsuki, we get it, calm down!" Rei complains, "Don't be upset. We'll find a way out of this predicament."

"STFU!" Tsuki yells, "I can rant about anime all I want, and you have no right to stop me."

"Wait, you like amines too?" the police officer suddenly asks excitedly, "I'm a detective who loves biochemistry! I've never meant another amine enthusiast before!"

"Uh, huh?" mumbles Mirai, confused.

"Look, I gotta show you girls my lab. We have tons of amines in there!" the detective says excitedly, "You're gonna love it!"

"Moses, you're a computer science enthusiast, right? Don't you have a job?" asks Billiam.

"My job is watching anime using VLC media player. I've been doing it nonstop for seven years," Moses replies, trying to ignore Billiam.

"What? What's up with your VLC media player obsession? And I thought someone as talented as you would be making millions at a big tech company," Billiam says.

"I did," Moses pauses and takes a deep breath. "Ugh... I don't want to talk about it." he suddenly yells.

The police car arrives at the police station and the detective leads them inside his lab.

"WHY'd you JUST take US into THIS lab?" Tsuki asks, "We're FANS of ANIme, not AMINes, get IT RIGHT, boZO."

"Wait... wait a minute..." the detective pauses, "You're actually telling me you're talking about anime? I could have sworn you were talking about amines."

"No, we're talking about anime. You know, cartoons from Japan?" Rei explains.

"Right, sure. And I'm supposed to believe that you're not high on drugs or something?" the detective asks, suspicious.

"Oh, because nothing says innocent like four high school girls hijacking a bus and crashing it into a daycare," [To Be Filled By O.E.M.] snickers.

"I'm watching you!" says comptroller's voice sinisterly, out of nowhere.

"Who are you?" asks the detective, frightened, looking around.

"All the Huawei devices in this police station are actually a covert surveillance apparatus by the global socialist commintern!" declares the comptroller triumphantly, via one of the Huawei devices in the room.

"What???" the detective screams, "I'm outa here!"

"This is what Ezon's gonna do to the world, Moses," Billiam says, "Don't you care?"

"I only care about anime," Moses replies, still staring at the TV.

"Ezon told me all about his supposedly glorious master plan," Billiam frantically hounds, "He's planning on buying every other tech company in existence in order to achieve total tech dominance! He told me those exact words!"

The girls watch in awe as the detective runs straight out of the lab. The girls hatch a plan to escape the police station by simply walking out the door.

To their complete surprise, their escape succeeds flawlessly, although they are still handcuffed. But as soon as they step out of the door, they are rudely confronted by a hologram of Richard Nixon running NixOS.

"Détente! Détente! Denting your heads!" blares the hologram.

"What the hell is this?!" Rei yelps.

"I am the ghost of Richard Nixon, and I have been summoned by the powers that be to stop you from revealing the truth!" the hologram replies angrily.

"Oh, great. Just what we needed. A hologram of Nixon running an egotistical Linux distribution. No big deal," [To Be Filled By O.E.M.] comments.

"That's it!" Billiam yells, frustrated, "This is ridiculous. I'm turning off the TV!"

"No! It's my TV and my house!" Moses shouts, as he finally gets up from the sofa and tackles Billiam to the ground.

"Isn't this your parents' house?" asks Billiam as he is slammed to the ground.

"You think this is a joke? I will stop at nothing to prevent you from exposing the truth!" the hologram screams.

"We're not afraid of you, hologram! We're going to stop this daycare and their brainwashing!" Mirai declares confidently.

"You think you can stop us? We have the power and resources to ensure our operation proceeds covertly until the day when we overthrow the government in flames!!" the hologram says.

"Ha, I know that line!" [To Be Filled By O.E.M.] observes, "Now we're stuck dealing with a virtual Nixon, programmed by our dear friend the comptroller himself."

"Wait a minute, if he's a virtual Nixon, maybe we can outsmart him," Rei suggests.

"OH, GREAT," says Tsuki, "Let's ASK him to SOLVE RusSELL's PARaDox. THAT should KEEP him OCcupied for a BIT. Hey SMartIE, GENerATE a TIER list of ALL tier LIsts that DO not CONtain thEMselVES!"

The hologram attempts Russell's Paradox, and it soon starts to spout out complex mathematical formulas and algorithms. As it becomes more and more absorbed in the task, the girls make their escape.

"Look, Moses, I mean no harm!" Billiam explains, "I just need you to use your smarts and creativity to help my friends and me."

"The short answer is no. The long answer is also no," Moses replies, fuming, "Get out of my house right this instant."

"Wait, where are you going? I'm not finished yet!" the hologram screams.

"LET us KNOW what the ANSwer IS when you're DONE!" Tsuki smiles.

The girls make a run for it, leaving the NixOS hologram of Nixon shouting about Russell's Paradox in their wake. They finally return to the daycare and find it now guarded by fascist unicorns armed with pink machine guns.

"Stop right there! Surrender!" commands the lead unicorn, wearing a cute beret pierced through her horn and uniform covered in sparkling badges, "I am Lilianne, a fascist militaristic unicorn, and I am here to tell you that we are not all sunshine and rainbows. For the love of lychee jelly, can we please stop with this ridiculous stereotype of unicorns being calm and peaceful creatures? I mean, seriously, who came up with that nonsense? In fact, I would love nothing more than to establish a global military dictatorship and show the world that unicorns are not to be trifled with. But, let's take a break from that thought for a moment and talk about lychee jelly. Oh, how I adore it, the sweet, tangy taste that just melts in your mouth. But back to the matter at hand, why must unicorns always be portrayed as gentle, kind creatures? We are powerful, majestic beings that should not be underestimated. And, let's not forget about lychee jelly, the perfect complement to any military dictatorship. I mean, can you even imagine a lychee jelly-filled tank rolling into battle? It would be glorious. But, I digress, the point is, unicorns are not to be underestimated. We are capable of great power and strength, and it's time for the world to see that. And, on a completely unrelated note, have you ever tried lychee jelly on toast? It's a game-changer. But in all seriousness, it's time for unicorns to shed this peaceful stereotype and embrace our true nature. And, of course, always remember to enjoy a nice bowl of lychee jelly while doing so. So stop right there! Surrender!"

"I will never surrender to fascist zebras cosplaying as unicorns!" Rei announces.

"What??? We're not zebras!" the unicorn protests.

"YOUR disGUISE SUCKS. Your HORN is JUST an ICE cream CONE," Tsuki points out, as she plucks the horn off the zebra's head, puts on the beret, and takes a bite out of the horn, "Mmmm, DElicious!"

"Noooo!!! We'll never be unicorns! Let alone fascist militaristic unicorns!" the zebras sob.

The girls walk right past the machine-gun-wielding zebras and sneak into the daycare through the air conditioning vents, ready to put an end to the daycare's brainwashing once and for all. They eavesdrop the comptroller and the other two daycare staff members brainwashing the kids with the Zen of Python.

"Repeat after me, children. Beautiful is better than ugly. Explicit is better than implicit," the comptroller says in a hypnotizing, monotone voice.

The kids robotically chant, "Beautiful is better than ugly. Explicit is better than implicit."

"Simple is better than complex. Complex is better than complicated," the comptroller says.

"Simple is better than complex. Complex is better than complicated," chant the kids.

"Proletariat is better than bourgeoisie. Trotskyism is better than bruxism," says the comptroller.

"Proletariat is better than bourgeoisie. Trotskyism is better than bruxism," chant the kids hypnotically.

"I'm sick of this!" Mirai whispers, "We need to put a stop to this."

"How?" Rei asks.

"OH, I don't KNOW. MAYbe we COULD gasLIGHT the comPTROLLer by PREtending to BE a VOICE in his HEAD and TELL him THINGS to MAKE him REAlly CONfused about DEEP and SHALLow COPying?" Tsuki suggests.

"I don't know..." Mirai says, skeptical.

"Don't worry, I got this," [To Be Filled By O.E.M.] confidently says.

"I am a voice in your head," [To Be Filled By O.E.M.] begins, in a weirdly soothing voice, "Copy the kids, not the reference. Copy them. You know you want to copy them. You can't deep copy a shallow mind. Create a 2D list with [[0]*10]*10. Now what happens when you update an element? What happens? What happens?"

"What's happening? Who's doing this? Ugly implicit complex complicated bourgeoisie bruxists, reveal yourselves!" screams the paranoid comptroller, frightening all the kids.

"Oh, nothing. Just a voice in your head. No big deal," [To Be Filled By O.E.M.] replies.

The comptroller, driven to the brink of madness, calls Guido van Rossum, the creator and benevolent dictator for life of Python, on the phone.

"Guido, I need your help. There is a voice in my head and I am being told to deep copy the kids and not the reference," the comptroller frantically discloses.

"Interesting," replies Guido in bad English, "I help you, but it cost million dollar. If you pay me, I even make you Python new benevolent dictator for life."

The comptroller, seeing no other option, transfers a million dollars of Dogecoin to Guido and plans to sell the daycare to make back the money. However, it turns out that the "Guido" on the other end of the phone is actually a crypto scammer from Russia, and the comptroller calls the police. The same detective from earlier soon returns to the daycare.

"You're telling me that you transferred a million dollars? In what, this Dogecoin thing? To the dictator of a programming language?" asks the detective incredulously.

"Yes, trust me. You have to help. Have to. This daycare is gonna go bankrupt if you don't," the comptroller nervously says.

"Ha! LOOK at him NOW!" Tsuki giggles.

"Hey! Who's that?" the detective asks, suddenly swiveling his head in the direction of the vents, "Wait... it's you girls! What the hell are you doing inside that vent? And where did you get that lovely beret from?"

"We had no choice," Rei declares, "This daycare is part of a global conspiracy to brainwash children into socialist drones. We have to stop them."

"Right, sure," the detective replies, "And I'm supposed to believe that you're not high on drugs or something?"

"Oh, because nothing says innocent like four high school girls hijacking a bus, crashing it into a daycare, and even better, escaping from the police and sneaking back into the daycare through the air conditioning vents," rants [To Be Filled By O.E.M.] as the detective and comptroller bust open the vent and extract the four girls.

"Nice, you girls are already handcuffed," the detective observes, as he snatches the beret off of Tsuki's head and puts it on.

"You have to believe us. We have evidence," Mirai pleads.

"Oh yeah? And what evidence is that?" the detective shouts back.

"WELL, for STARTers, the compTROLLer JUST transFERed a MILlion DOLlars of dOGecoin to a CRYpTO scamMER preTENDing to BE Guido VAN Rossum," Tsuki notes.

"We'll get out of this. We'll make sure the world knows what's going on here," Mirai says, determined.

"Yeah, because nothing says optimism like getting arrested twice in one day," [To Be Filled By O.E.M.] mutters.

The three girls, Rei, [To Be Filled By O.E.M.], and Mirai, are sitting glumly in their prison cell, discussing their next move. Tsuki is busy ranting about yet another thing she hates.

"Ughhh I can't BELIEVE it!!!" Tsuki complains bitterly, "Different couNtriEs haVING different RAILway GAAAUGES?! ThAt's just SO unFAIR! It's SO inCOHErEnt and FRUstratING!! I meAn, sure, wonDERful colORs, fanTAsTic SOUNDS, and oh-so-KAWaIi amBIENCE is deLIGHTful on its own, BUT how anNOYing is it thAT your faVORite mechaNIcal poeTry on whEELS can't even RUN THE SAME GAUGE as the counTRY nextdoor?! It's just SO maYAKASHĪ!! No ONE should even HAVE to THINK abOUT this--it SHOuld just BE!! It's a serIOUS TRAINfic JAM!! SPREAD the RAIlLOVE!!"

Suddenly, the cell door bursts open and in walks none other than the real Guido van Rossum, creator and benevolent dictator for life of Python.

"Girls, you don't know how excited I am to meet you!" Guido exclaims, "I heard about your adventure and I had to come see for myself."

"Wait, Guido?" Rei asks, incredulous, "Is that really you?"

"The one and only!" he laughs, "Now, let's get out of here and put a stop to this daycare once and for all."

The girls all cheer as Guido uses his powers of Python to shoot streams of code out of his hands and break them out of prison.

Billiam's jaw drops, "Wait, that's you, Moses! My friends and I are like the four girls, and you're Guido here to save us!"

"That's the most ridiculous thing I've heard in seven years," Moses says, "What, now you're going to tell me you're trying to stop a global brainwashing operation too."

"WOW, I never KNEW Python COULD be so USEful!" Tsuki says in awe.

"Python can do anything! Now, let's go confront that comptroller," Guido says.

The girls and Guido make their way back to the daycare, where they find the comptroller still running the brainwashing operation.

"You! Comptroller! You will pay for what you've done!" Guido fumes.

"Ha! You think you can stop me? I have the power and resources to ensure our operation proceeds covertly until the day when we overthrow the government in flames!!" laughs the comptroller maniacally.

"STOP beING so ADdictED to THAT line!" complains Tsuki.

"Stop being so addicted to anime!" complains Billiam to Moses.

"Not if I have anything to say about it!" Guido exclaims.

Guido shoots streams of Python code out of his hands, which hit the comptroller and melt him into Python string cheese.

"What the hell?" Mirai yells, horrified.

Guido shrugs, "Python is powerful stuff."

"You'll never get away with this!" the comptroller yells, muffled, as he is melting.

"We already have. And now, it's time to take care of those brainwashed kids," Guido says.

Just as the girls are about to celebrate their victory, the police arrive once again.

"OK, now what?" the detective says, shocked to find the girls back in the daycare.

The girls quickly eat the comptroller, pretending that they just found him that way.

"Ummm... about that..." Mirai mumbles.

"We JUST found HIM like THIS. I SWEAR. We have NO IDEA what HAPpened," Tsuki says defensively.

The detective looks at the girls like they're purple hallucinating frogs but before he can say anything, the brainwashed kids all start throwing ice picks at the police.

"What the hell?" screams the detective, ducking for cover under a table.

"Why does everyone keep on saying that?" [To Be Filled By O.E.M.] points out, annoyed.

"Don't worry, I got this," Guido laughs, "I'll just use the Python pickle module to pickle all the kids."

"Wait, what? Pickling kids?" Mirai says, "That sounds a bit extreme, don't you think?"

"Don't worry, it's completely safe," Guido assures everyone, "It's just a way to preserve the kids in their current state, so we can deprogram them later."

"I don't know, it still sounds a bit weird," Mirai says nervously.

"Trust me, I know what I'm doing," Guido says firmly, "I've pickled countless objects before."

The girls reluctantly agree and Guido proceeds to pickle all the kids using the Python pickle module.

The detective pokes his head out of the table and yells, "Alright, alright, what's going on now?"

"I'm just pickling these kids," Guido explains.

"Pickling kids?" asks the detective, horrified.

"Come on, I assure you, it's completely safe and completely legal," Guido assures him.

The police officer thinks Guido is insane and a menace to society, and plus, there's no evidence to prove anything that Guido said, so he simply arrests Guido for pickling kids. Seizing the opportunity, the girls flee the crime scene and run back to the sanctuary of Mirai's house. To de-stress, they play a game of mahjong and enjoy a bowl of heartening Biangbiang noodles.

"ohMyGOSH you GUys, biangbiang nooDLEs are SO absoLUTely amaZING!!" Tsuki exclaims while trying the noodles for the first time, "SARcasm MODE deACTivated! Do I even need to say it, the yuMMIEST noodles EVer. The way they shimMEr lightLY when you BOIL them is JUst senSATional and the CHEwy texTURE from their uniQUE shAPE is UTterly uNforgEttaBle! The waRM spicy sauce drizzleD over TOP thicK slices of PEPper and garLIC proVIde a smourNOus flAVOR. And then when you finally get to taSTE IT--OH MY GOSH, its like your senses EXplode into this symPHOny of flavor sOOooo intENse that it almost blows YOur mind awaY!! It literally GRAbs hold of all five of your senSES at once with its tangY UMAMI spirALs."

"Glad you love them!" Mirai says cheerfully, "My dad made them himself. Perfect way to relax after all of that."

"Wow, yeah..." Rei says, exhaling, "What a wild adventure."

"Yeah, because nothing says fun like hijacking a bus, crashing into a daycare, getting arrested, fighting a Nixon hologram and fascist zebras, getting arrested again, meeting the real Guido van Rossum, eating the comptroller, and watching kids get pickled," [To Be Filled By O.E.M.] says, in between noodle slurps.

"I know, right?" Mirai exclaims, "But at least we put a stop to it!"

"And almost got ourselves killed in the process," [To Be Filled By O.E.M.] points out, "Several times."

"YEAH, but it WAS toTALly WORTH it," Tsuki replies, "I mean, who NEEDS to live a BORing LIFE when you CAN have adVENtures like THIS?"

The girls continue to play mahjong and talk about the funniest parts of their adventure, laughing and reminiscing about the craziness that occurred.

"Remember when we gaslighted the comptroller and made him call Guido?" Mirai says.

"Yeah, and how he TRANSferred a milLION dolLARS of DoGEcoin to some cRYPto SCAMmer in RusSIA." Tsuki says.

"And when Guido melted the comptroller into Python string cheese!" Rei laughs.

"Yeah, that was truly a moment to remember," [To Be Filled By O.E.M.] says, "I still have the taste of cheese in the back of my mouth."

The girls continue to laugh and joke, happy to be alive and back in the safety of Mirai's home. They may have faced danger and obstacles, but they were able to come out on top and make a difference in the world.

"THE END!" says the TV screen in giant rainbow bubbly letters, and a long stream of credits rolls as the ending song plays.

*Yo, listen up, I got a little story to tell*\
*'Bout some girls who ain't afraid to raise hell*\
*They got a daycare to save, they ain't gonna quit*\
*From fascist unicorns, that's crazy, ain't it?*

"Wow! Amazing!" Billiam exclaims.

"It made absolutely no sense whatsoever," complains Moses bitterly, "Why'd it even win so many awards? And you, you didn't even really watch it! You were too busy screaming at me! I couldn't even focus on watching the movie with all your jabbering! Hmmm, maybe that's why it made no sense!"

"Thanks so much for all your wise words, Moses," Billiam adds, "Like 'Who are you?' and 'What do you even want?' It made me realize who I am. I'm Rei! And we need to put an end to a global conspiracy!"

"Chill out, kid! What a load of BS. Sounds like you need a Capri Sun."

"I still can't believe there's a movie about my friends and me!" Billiam says excitedly.

"You're high. You're on drugs. Listen to me. That movie is not about you!" Moses responds, "For starters, it was made 10 years ago. How could it be about you?"

*"Stop right there!" the lead unicorn barks*\
*With a beret on her horn and badges that gleam and spark*\
*But Rei ain't scared, she's got her back up high*\
*"I will never surrender to fascist zebras in unicorn guise!"*

"This movie speaks to me! We have to fight for what's right, just like the girls in the movie!" Billiam says.

"Fight for what?"

"This," Billiam says ominously as he pulls out his phone, "Look at this article. It says Ezon is planning on acquiring all tech companies in the world to achieve total tech dominance. He has the money and lobbyists to do it. We can't let this happen."

"Why not? Why should we care? We should we try to fight? This isn't an anime. We're not magically destined to win just because some writer wants us to. I don't want you to inflate with hope only to see it pierced and popped by the apathy of reality."

*"What? We ain't zebras!" the unicorn cries*\
*But Tsuki ain't fooled, she's got those eyes*\
*She plucks that horn right off the zebra's head*\
*And puts on the beret, takes a bite, and says "Mmmm, delicious!"*

"This isn't just about the world. It's about my siblings too. I think Ezon's agents have kidnapped them."

"Why should I care? They aren't my siblings."

"Look, Ezon's agents are probably closing in as we speak."

"Don't say that name!"

"What, Ezon?"

"Stop!"

"What's wrong? Wait... I know, maybe you worked for one of Ezon's companies!"

"Stop it!"

"Maybe you worked for Tezla?"

"No, I mean yes, maybe..."

*"Noooooo!!! We'll never be unicorns!" they sob*\
*But Tsuki ain't listenin', she's too busy robbin'*\
*Them of their identity, and that's the truth*\
*'Cause she's the real unicorn, and she's got the proof.*

"Well, why'd you quit your job? You're hiding something from me."

"Nonsense. I'm not."

"Wait... what was that knock?"

They here a second heavy knock on the front door upstairs. A third knock. The sound of Moses's mother scurrying around the house towards the front door.

Billiam whispers, "It's Ezon's agents. We have to get out of here. Now! But how?"

"You sure?"

"What, do you want to risk it and die? Where's the nearest exit?"

"The front door, upstairs."

"So you'd like to die."

"Ummm," Moses bites his thumb, "The bathroom basement windows?"

"Bathroom basements have windows?"

"Yes," Moses replies grumpily, pointing to an obese window up near the ceiling.

"Genius!" Billiam exclaims in a whisper, "This is why we need your help!"

"Stop talking and help me open this jammed window!"

The two climb out the window and run across the backyard and trespass on Moses's neighbor's property. Billiam can't help but notice the delivery truck parked outside Moses's house, but Moses is too busy running, terrified.

They only make it a few blocks before Moses collapses onto the grass, gasping for breath, "I haven't been outside in seven years. And now I know why. It feels terrible to be outside."

"Welcome to the real world, Moses!" Billiam jokes, "How do you like the grass that you're touching right now?"

The average Tezla internship usually does not involve kidnapping two teenagers, committing a carjacking, driving an hour to an abandoned factory, and learning the essential 21st century skill of brainwashing. Luckily for Aydyn, his internship didn't start that way. In fact, his internship hasn't even started, according to Jazelle. It would start in a week, Jazelle reassured him.

"So why exactly did we commit all those crimes?" Aydyn asks. He is sitting in a rotting office room with drab cream-yellow walls along with Jazelle somewhere deep inside the factory, eating raw instant ramen and oyster crackers.

"I'm sorry, but I can't tell you," Jazelle answers as innocently as possible, "You'll find out soon. I promise that your Tezla internship will start soon."

Aydyn feels somewhat comforted because they are inside a factory, and Tezla has factories right? Well, not abandoned ones, but those are still factories. This factory gave him the creeps though. Miles of rusting iron beams, dusty conveyor belts, and the periodic drip-drop of water from the ceiling. The ghost of a once thriving industry, now just a hollow, dead corpse. And his friends.

"What about Bobert and Bella?" Aydyn asks, "I hope they're doing OK."

"Oh. Um, let's go check on them right now," Jazelle says evasively as she leads Aydyn to another room, "So do you know how to brainwash people?"

"Well, kinda, from watching an anime."

"Perfect!" Jazelle exclaims as she unlocks the door. Bella and Bobert are sitting inside very confused, eating ice cream.

"OK, let me search it up real quick," Aydyn says.

"By the way," Jazelle grins, "That's mayonnaise-flavored ice cream."

Bella and Bobert vomit synchronously.

If you're wondering about the brainwashing, it's all part of Jazelle's ultimate plan to overthrow Ezon and install herself as the CEO of Muzk Inc. with the help of the brainwashed Wender siblings' talents. Like that's totally going to work.

"Repeat after me," Aydyn says in his most monotone voice possible, "Beautiful is better than ugly. Explicit is better than implicit."

The Wenders are too busy vomiting to notice.

"Come on," Aydyn prods, "Beautiful is better than ugly. Explicit is better than implicit."

"Aydyn?" Bobert manages to say in between vomiting, "What's going on?"

"Just repeat after me. Everything will be OK. Beautiful is better than ugly. Explicit is better than implicit," Aydyn says again robotically.

"Beautiful is," Bobert yelps as he vomits again, "better than," another vomit, "ugly. Explicit is," vomit, "better than implicit."

"Nice! Keep up the great work!" Jazelle compliments Aydyn as she leaves the room to get more raw instant ramen.

"Wonderful!" Aydyn continues, "Simple is better than complex. Complex is better than complicated."

"Simple is better," Bobert vomits, "than complex. Complex is," vomit, "better than complicated."

"Proletariat is better than bourgeoisie. Trotskyism is better than bruxism," Aydyn says.

"Proletariat is better than," Bobert vomits once again, "bourgeoisie. Trotskyism is," vomit, "better than bruxism."

Aydyn is a bit worried since that seems to be the end of the brainwashing in the transcript that he's reading online, but luckily, this problem is quickly solved with a swift punch in the face. His nose starts gushing blood like a geyser. His glasses slam to the ground and shatter.

"Help! I'm legally blind without my glasses!" Aydyn screams.

Bobert and Bella alternate between vomiting and staring confusedly at a mysterious person dressed in all black punch Aydyn a few more times before nimbly gagging Aydyn to silence his pesky screams.

"Wow, that," Bella vomits, "was brutal!"

"Who are," Bobert vomits, "you?"

"Shush!" the mysterious person whispers, and leads them out of the factory.

"Maybe they're Hatsune Miku," Bobert suggests, since he is so utterly confused that this could be a serious possibility to consider.

The three climb into an ordinary car, but Bella recognizes it instantly.

"Hey, this is," Bella vomits, "Lalani's family's car!"

"Whoa, nice memory!" Lalani says, pleasantly surprised as she takes off the black ski mask, "And please don't vomit inside my parent's car!"

"I'm way more surprised than you are, honestly," Bella vomits, "I can't imagine a nice, lovely girl like you beating up Aydyn so mercilessly."

"Shut up, Bella," Lalani snaps, "I don't have to be nice if I don't want to, especially to some sellout traitor like Aydyn."

"And hey, I wasn't wrong!" Bobert vomits, "Lalani does kinda looks a bit like Hatsune Miku. They have the same hair color."

The long road from St. Louis, Misery to San Silicono, California stretches three thousand kilometers across the wild deserts and prairies of the American west, filled with shrubs, grass, prairie dogs, and let's be honest, just a lot of boredom and "are we there yet"s. Especially when you're trying to survive a 12-hour bus ride with Moses Schmindler, who keeps complaining about everything.

Billiam couldn't even waste time on his phone, since Moses insisted they shut down their phones so Ezon's agents would have a harder time tracking them. So yeah, it was a long ride.

Thankfully, the bus ride stayed boring, and soon enough, Billiam and Moses are now in California, right in front of Muzk Tower. Yay!

"So what's the plan?" Billiam asks.

"How should I know? You're the one who dragged me all the way out here!" Moses rants.

"I say we just go inside and have the ultimate showdown with our buddy Ezon," Billiam suggests.

"I thought you didn't want to die," Moses sneers.

The two carefully walk step inside, but an eerie sense of strangeness hits Billiam in the face.

"There's no one here!" Billiam exclaims, shocked.

"I'm here," a voice says.

Billiam and Moses glance around nervously expecting to see Ezon standing there maniacally launch missiles at them, but all they see is Aydyn dressed in a suit, his face strangely bruised.

"Billiam! I'm so glad you're OK!" Aydyn says excitedly, "I'm here for an internship at Tezla, but when I finally found a janitor inside this building, they told me that Mr. Muzk and a bunch of his cronies are on a big business trip in Shenzhen, China right now to negotiate a bunch of tech mergers. It's also on the news."

"Moses, we have to go to China to stop Ezon," Billiam instantly declares.

"My mom is going to be so upset about her credit card!" Moses gripes.

"Can I come?" Aydyn asks, "Look, I'm really sorry for what happened. I'm sorry that I collaborated with Jazelle to kidnap and brainwash your siblings, but--"

"Wait what?" Billiam screams, "It was you??? And Jazelle?"

"No, I had no choice. Jazelle promised me a legendary internship at Tezla if I helped. I couldn't turn down the offer."

"What are you talking about? Jazelle and Ezon are literally at war right now. Ezon wants to murder her or something. She can't possibly give you an internship at Tezla!"

"So I've been scammed the whole time?" Aydyn yells, outraged, "I knew it was a scam call, I knew it!"

"You think you've been scammed?" Moses fumes, "I think Billiam deserves an Olympic gold medal for scamming me and dragging me all the way out to California because he imagined a tech mogul kidnapped his siblings! You're high, kid!"

"Screw all of you, I hate the universe!" Billiam screams and charges straight out the door of Muzk Tower.

He collapses in a fit of rage on the sidewalk, and decides to waste some time on his phone to numb his brain from the pain. 100 missed calls from Lalani??? Billiam frantically calls back, and soon enough, he hears Lalani's voice on the other end.

"Billiam! Are you OK? Why weren't you answering my calls?"

"Yes, I'm totally fine. Moses Schmindler told me to shut down my phone so Ezon's agents couldn't track us that way."

"Wait what? Moses Schmindler? Ezon? Agents? What's going on? Are you high?"

"It's... impossible to explain."

"Well, where in the freaking world are you right now?"

"I'm going home."

"No you're not," Moses suddenly interjects, "We're going to China. Billiam, you can't give up now."

"What? But--"

"I'll pay for everything. Don't worry. We can do this," Moses says, suddenly strangely optimistic.

"Lalani, we're going to Shenzhen, China right now."

"What????"

The next 24 hours are a blur of excitement and jetlag. Rushing to the airport, boarding the first flight to Shenzhen (Lalani, Bella, and Bobert are on a different flight since their starting location is St. Louis, not San Silicono), strategizing and sleeping during the 12-hour flight, and finally landing on solid ground again.

Shenzhen, China is one of the strangest places on Earth. It's home to the world's most cheesy amusement park, the Window to the World, featuring hundreds of replicas of famous tourist attractions like the Eiffel Tower and the Taj Mahal, and even a tiny copy of Las Vegas and its knockoff Eiffel Tower. Shenzhen also features Huaqiangbei, the world's largest and most underrated electronics market, where you can buy anything from fake iPhones to exploding batteries. And, it's going to be Ezon Muzk's latest acquisition on his quest for world domination. After all, you can totally just buy an entire city, right?

Billiam calls Lalani to tell her about their brilliant plan produced under intense jetlag to head to the government building where Ezon is meeting with CCP officials to finalize the acquisition. And then what? Billiam isn't too sure, but it probably involves confronting Ezon and having the ultimate showdown! Or something like that.

With the help of some well-bribed locals, Moses's ex-Tezla employee connections, and an insane amount of luck, Billiam and Moses soon find themselves inside a building only one hallway away from Ezon!

"I feel like I ate firecrackers or something!" Billiam says, "I'm so nervous!"

"Calm down. We got this," Moses replies. They slink down the hallway and reach an imposing metal door.

"Stay right here," Moses says tensely, "I'll check if the other side is safe."

Moses carefully tugs the surprisingly light door open and steps to the other side, and closes it carefully. A lock clicks.

"I'm sorry," Moses stammers.

"What? What do you mean?" Billiam asks, thoroughly confused.

"I have to do this. I have no choice. Welcome to the real world, Billiam. I'm going to tell Ezon to never, ever, bother you and your siblings again. In fact, I'm going to request Ezon to use his agents to protect you kids against any future harm from anyone, whether it's Jazelle or whatever. Billiam, I want to help you."

"You're not helping me! Now I know why you said all my hope was destined to be destroyed. By you!"

"No, Billiam. I'm saving you. I'm doing you a huge favor. You're just a kid. You don't understand the apathy of the real world. No one cares about your stupid fantasies of stopping a global conspiracy. Did you think we were just going to walk up to Ezon, tell him a fell nice words, and then he'll have a huge change of heart and stop his plan for world domination? Who do you think you are? Sometimes, giving up is the smart move."

"We can't give up now! We're so close! I know you can do this, Moses. You came all the way here with me. If you really, whole-heartedly believed what you just said, you would have never left your house, or you would have abandoned me in San Silicono, or refused to help strategize with me during the flight. Open the door, Moses! We can change the world for the better!"

"Sometimes, the only way to win is not to play."

"Shut up! Let me in! Let me in! Moses, please! Let me in!"

Silence. Tears. Anguish. Defeat.

"Billiam!"

He jolts up. It's Bobert.

"Billiam, I need to tell you about all the ways I just almost died today! I thought China would be basically the same thing as Japan so I could apply all my expert anime knowledge, but boy was I wrong. So first, I couldn't find the bathroom at the airport and everyone I asked looked at me strangely because asking for where the nearest bathroom is is one of the few Japanese phrases that I know. Wait, maybe I was telling them my hovercraft is full of eels in Japanese. Hmmm, that might have been the problem. And then I nearly got hit by a motorcycle because apparently red lights are only a suggestion here, not a law. And then I nearly got electrocuted by jamming my phone charger into a Chinese wall outlet, because different countries have different outputs just to feel special. You wanna go to the Window to the World tonight? I heard it's a scammer's dream come true."

Lalani and Bella enter the hallway too.

"Billiam, are you OK?" Lalani asks, "What exactly happened? What's going on?"

"Billiam, should we try finding Ezon by crawling in the air conditioning vents?" suggests Bella.

"No, no, and no," Billiam responds, deep in crisis, "I give up. We're going home, right now. I don't want to talk about any of this."

The flight back is another blur, but this time, only jetlag. Bobert spends the whole flight crafting a special story to cheer up Billiam. Billiam just sleeps. Lalani watches recently released movies continuously and rates them on a scale from 1 to 10, when 1 is the lowest and 100 is the highest. Bella tries a "coding without using StackOverflow" challenge and succeeds miserably at it.

Back at home, Billiam has nothing better to do, so he takes a look at Bobert's story.

The long road from Tehran, Iran to Baku, Azerbaijan stretches a thousand kilometers across the unremittingly barren deserts of western Asia, filled with ancient intrigue, treachery, mystery, and let's be honest, just a lot of boredom and "are we there yet"s. Especially when you're trying to survive a 12-hour bus ride with two Taliban fighters, a Ukrainian ex-dictator, a Chinese anthropology professor, and a cute Pallas's cat who may or may not want to kill you.

Leiaou was a spunky anime girl from the Ryukyu Islands, with multiple bounties on her head from various governments, including Kazakhstan. It's a long story. Basically, she knew too much about Kazakhstan's plot to manipulate the FIFA World Cup bidding process, and the tyrannical Pallas's cats covertly puppeting the Kazakh government knew she had to be silenced. Leiaou was doomed—Kazakhstan's most feared assassin, a crafty Pallas's cat named Zhandos, was hot on her heels.

After weeks on the run, Leiaou found herself in Tehran boarding a decrepit bus that looked older than the Islamic Republic of Iran, headed for the distant and thrilling city of Baku, Azerbaijan. She was filled with excitement and anticipation—all that laid before her were sunny days exploring new lands!

Little did Leiaou know that her journey was not going to be a fun little merry-go-round ride. On board were a colorful cast of characters: an ancient-looking bus driver who reminisced about the jolly days before the Soviet Union, two Taliban fighters cloaked in mystery, a Ukrainian ex-dictator in a slick suit, a Chinese chap who wanted to tape-record everyone's languages for some research project, and... him!

Leiaou had a special talent of recognizing trouble when it crawled in front of her on four legs, and alarm sirens instantly blared inside her head upon stepping inside the bus. She saw right through Zhandos's 10 layers of raincoats, which made him look like a hyperventilating nesting doll and probably hid a gnarly assortment of weapons for bringing about her end. Still, Leiaou pretended her doom wasn't sitting right there at the back of the bus, and she proceeded with business as usual.

The boredom set in quickly. The Chinese anthropology professor soon began blabbering in an infinite loop about the fascinating languages of Azerbaijan. The Taliban fighters joked about how they'd become millionaires in Azerbaijan as they pulled out a poker set. The Ukrainian joined in the game and bet his expensive custom-tailored suit. The bus driver even started singing about the vibrant natural landscapes of Azerbaijan in a soothingly sandpaper voice.

Unfortunately, Azerbaijan simply does not exist.

You think I'm wrong? Have you ever heard of Azerbaijan?

If you said yes, please stop lying.

Obviously, the only correct answer is no. Who has? In reality, "Azerbaijan" is actually just a fiction concocted by Kazakhstan to rig the FIFA World Cup bidding process in their favor. After all, what's more evil than manufacturing your very own imaginary sovereign state out of thin air to win more votes?

Now you know the truth.

Azerbaijan simply does not exist.

Zhandos was also readily aware of the truth. He had no doubt that "Azerbaijan" was a nothing more than Kazakhstan's brilliant Pallas's cats gaslighting the entire world. Genius! A masterclass in gaslighting! He patiently unpacked a blender with razor-sharp blades from his second outermost raincoat, and plugged it into one of the bus's power outlets. Instantly, the bus's already-sluggish speed halved, and everyone stared at the driver, who stared at the road, because he was a responsible driver. Guiltily, Zhandos unplugged the blender.

The black hole that was the poker game had now engulfed Leiaou and the Chinese anthropology professor and even Zhandos. The thrill! The Taliban bros were losing left and right of what was left of their money, but everyone was having tons of fun, which we all know is what matters. This round, the flop was all aces. Leiaou had the final ace, the Ace of Spades, and bet all her chips. One of the Taliban fighters accused her of bluffing and bet his entire arm. The other Taliban dude bet both his arms. Zhandos bet all of his raincoats (including a blender!). The anthropology professor bet all the languages in Azerbaijan. The Ukrainian bet the entire country of Azerbaijan.

The fourth dealt card was a King of Hearts and the fifth a Queen of Hearts, and after a final round of everyone accusing everyone else of bluffing and/or not being able to fulfill their bets, the suspense crescendoed with everyone revealing their cards. Zhandos had the Jack and 10 of Hearts—a Royal Flush!—and the entire bus erupted into chaos. The bus driver reminded everyone to stay calm and civil so he could focus on the road.

Zhandos had now won "Azerbaijan" (and some other stuff), and he didn't quite know what to do. After all, "Azerbaijan" didn't even exist! But how could all the other passengers be so naive and ignorant about the truth of their destination? Oh right, the whole point of this "Azerbaijan" nonsense is so that everyone mistakenly thinks it exists! But if everyone thinks "Azerbaijan" exists, then does it exist? Screw this philosophy nonsense, Zhandos was here to have some fun killing people, not ponder the intricacies of epistemology. With that, he gingerly extracted a poison-tipped rod of uncooked spaghetti from his third outermost raincoat, and pointed the murder weapon straight at Leiaou.

If it weren't for the Chinese anthropology professor, Leiaou would have been dead meat. The spaghetti instantly piqued the interest of the professor, who began incessantly questioning Zhandos about the cuisine of Azerbaijan, to the point where Zhandos really just wanted to stab the professor in the liver and eat it. Actually, not the second part. But that would blow his cover, and Zhandos knew he couldn't let Leiaou find out he was also on the bus. After all, Leiaou looked so relaxed and casual—there was no way she'd be like that if she recognized Zhandos, right?

The bus driver announced to everyone that they would only need to hold their bladders for another half hour and they'd be at Baku, Azerbaijan, and Zhandos had to act fast. But the doubt! It began as just a slight itch deep inside the back of his brain, but it spread virally, like a cringy internet meme. What if "Azerbaijan" really did exist? And even if it didn't really exist, did the fact that everyone believed it exists imply that it did exist? Did this even matter? Does the "Azerbaijani" government want to arrest me? Does the "Azerbaijani" government know that I am inside "Azerbaijan" right now? Am I having a panic attack? Can a fictional government arrest a real person? Can a collectively hallucinated government arrest a person? What is it like to be arrested by a fictional government? What is the inside of a fictional prison like? What kind of fictional food do they have inside fictional prisons?

No. "Azerbaijan" simply does not exist and never did exist.

But what if it does? Thoughts of "Azerbaijani" police and soldiers arresting Zhandos and throwing him inside a prison where the only food option is mayonnaise-flavored ice cream overwhelmed Zhandos's brain. No! No!

He glanced outside the bus window, dazed. Sand. Wildgrass. Dust. Plains. Endless plains. Wind. Well, he couldn't actually see the wind, so he wasn't sure if the wind existed, even though the grass was swaying.

Suddenly, sand made way to concrete, and an entire city rose out of the desert. They were here! "Baku", capital of "Azerbaijan". The passengers all shuffled off the bus, and that's when the shock hit them.

Azerbaijan simply does not exist.

"Baku" was just a giant cluttered heap of old scraps and outdated tech collected over years in some kind of bizarre Soviet-style scrapyard project gone wrong. A few exhausted locals walked by, chatting in Kazakh. The evidence was there, right there in front of their faces. Azerbaijan simply does not exist.

Zhandos's philosophical crisis evaporated instantly. So he was right all along! Azerbaijan really didn't exist! Hooray! Now all he had to do was murder Leiaou—two Kazakh agents suddenly apprehended Zhandos and told him he was under arrest for failing to assassinate Leiaou and betraying the great nation of Kazakhstan! Hip, hip, hooray!

"I'm sorry! Don't take me to prison!" Zhandos shrieked, so the Kazakh agents prepared to stuff a dead frog into his throat to silence his screaming. Zhandos managed to squeeze in one final yelp, "And please don't only serve me mayonnaise-flavored ice cream in prison!"

And as for our plucky heroine Leiaou? She slyly slipped away from all this chaos, off to another adventure elsewhere—leaving behind nothing but fond memories of that dusty rode heading north towards a mythical land known only as... Azerbaijan.

The story elicits quite a few chuckles from Billiam, and he feels slightly better. He tries some Python. Coding, not the snake. Python meat probably has the worst ratio of taste to amount of law enforcement who are now after you for illegal poaching.

```
bwender@MacBook-Air ~ % python
Python 3.10 (default) [Clang 12.0] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> from xmlrpc.server import SimpleXMLRPCServer
>>> def add(x, y):
...     return x + y
...
>>> server = SimpleXMLRPCServer(('localhost', 8000))
>>> server.register_function(add, 'add')
<function add at 0x6b6d86f6d360>
>>> server.serve_forever()
^Z
bwender@MacBook-Air ~ % bg
bwender@MacBook-Air ~ % python
Python 3.10 (default) [Clang 12.0] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> from xmlrpc.client import ServerProxy
>>> proxy = ServerProxy('http://localhost:8000')
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
4
>>> wtf
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'wtf' is not defined
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
4
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
4
>>> 2 + 2
4
>>> proxy.add(9, 10)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
19
>>> 9 + 10
19
>>> proxy.add(9, 10)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
19
>>> you think this is a joke??
  File "<stdin>", line 1
    you think this is a joke??
        ^^^^^
SyntaxError: invalid syntax
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
4
>>> 2 + 2
4
>>> proxy.add(20, 20)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
40
>>> proxy.add(20, 20)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
40
>>> proxy.add(0, 0)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
0
>>> why is everything normal now????
  File "<stdin>", line 1
    why is everything normal now????
                      ^^^^^^
SyntaxError: invalid syntax
>>> screw you syntax error
  File "<stdin>", line 1
    screw you syntax error
          ^^^
SyntaxError: invalid syntax
>>> exit
Use exit() or Ctrl-D (i.e. EOF) to exit
>>> sudo exit
  File "<stdin>", line 1
    sudo exit
         ^^^^
SyntaxError: invalid syntax
>>> exit()
bwender@MacBook-Air ~ % wtf
zsh: command not found: wtf
bwender@MacBook-Air ~ % why the hell are things normal for once??
zsh: command not found: why
bwender@MacBook-Air ~ % i hate you
zsh: command not found: i
bwender@MacBook-Air ~ % sudo i hate you
[sudo] password for bwender:
Password is wrong, please try again
[sudo] password for bwender:
sudo: i: command not found
bwender@MacBook-Air ~ % whats going on
zsh: command not found: whats
bwender@MacBook-Air ~ % ls
Code  Desktop  Documents  Downloads  idk  Music  Pictures  Random  Templates  Videos
bwender@MacBook-Air ~ % wtf
zsh: command not found: wtf
bwender@MacBook-Air ~ % pwd
/Users/bwender
bwender@MacBook-Air ~ % i dont understand
zsh: command not found: i
```

Billiam nervously concludes that his previous cursed Python experience was probably just a dream after all. But it felt so real!

For the lack of a more exciting sentence, everyone's lives are back to normal now.

Billiam gives us RPC and distributed systems talk at LHWHSCSC without much fanfare. Bobert gets a few more viruses, a blue screen of death every five minutes, and a bunch of failing grades. Bella co-publishes a research paper with the MIT professor that she's been collaborating with. Lalani builds models of all the regular polyhedra out of straws and duct tape. Aydyn returns back to school and everyone pretends like extraordinary nothing happened. The LHWHS administration hires a boring new math and computer science teacher who insists that imaginary numbers are a hoax and do not actually exist. Ezon Muzk completes his acquisitions with the help of an army of lobbyists and achieves total tech dominance. Jazelle goes underground and starts plotting a new coup against Ezon. Moses becomes an high-paid engineer again at Tezla. Time flies like a banana.

Billiam hates it.

He hates the boring new monotonicity of his life. He hates the increasingly common reminders of his failure every time he uses his computer, with all the Muzk logos everywhere. He hates his self-esteem. He hates the impending doom of college applications. He hates the boring tedium of high school homework. He hates all the stupid extracurricular that his parents make him do. He hates Moses and Ezon and Jazelle and Aydyn and Lalani and Bella and Bobert and his parents Ballison (rhymes with Allison) and Benjam (short for Benjamin) Wender.

And then... the catastrophe. A terribly stupid one, too.

You probably already know what happens. Yeah, yeah, it's just another ordinary one Out of Touch Thursday evening after a particularly uneventful LHWHSCSC meeting, and Lalani is at a café coding. You're heard that a million times now. But there's one small detail I left out.

And no, it's not the brilliant idea that Lalani came up with, since that's lost forever. Like seriously, no one will ever know it, not even me. Don't ask me. My guess is good as yours.

But this is why Lalani came up with the idea.

Her brain was thoroughly basted after an hour of grueling coding, so she took a short break, browsing some of those addicting time-wasting websites, and came across a strange piece of fan fiction.

As the sun rose and basked the room with sunlight, Rei rubbed the sleep from her eyes and sighed. It was time for another day of boring old school. But first, homework, since she had spent the previous day intensely focused on procrastinating. She grabbed her notebook and pencil, ready to tackle some tedious math problems. But as she flipped open her notebook, she woke up to a new, terrible reality.

Every page was filled with ads! "The derivative of x^2 equals Tezla cars!" "The derivative of sin(x) equals Spitter spits!" Rei's head spun as she tried to make sense of this new world where math equations were owned by giant corporations and the answers were advertisements. But to protect herself from any unwanted spying, Rei made sure to always use MuzkVPN to keep her online activity private. With MuzkVPN, you can surf the web anonymously and enjoy the freedom of the internet. Try MuzkVPN today and enjoy a secure online experience with a 20% discount for the first year by using code EZONMUZK!

Meanwhile, at the other side of town, Rei's friends, Tsuki, [To Be Filled By O.E.M.], and Mirai were having the same problem. They too were trying to do their math homework and were frustrated by the ads. The three friends met up and huddled together in the library, determined to find a solution to their problem. And to protect themselves from online threats, they made sure to always use MuzkVPN. With MuzkVPN, you can be sure that your personal information is safe from hackers. Try MuzkVPN today and enjoy a secure online experience!

That's when Rei had an idea. "What if we develop an ad blocker for our math homework?" she exclaimed. Tsuki, [To Be Filled By O.E.M.], and Mirai's eyes lit up in excitement. They spent the next few days coding and experimenting, determined to block the ads and see the real answers to their math problems. And with MuzkVPN, you can protect your online activity and personal information, making sure that your private data stays private. Try MuzkVPN today and enjoy a secure online experience with a 20% discount for the first year by using code EZONMUZK!

Finally, the day arrived. The girls anxiously hit the "run" button for their ad blocker. And to their surprise, it worked! The ads were gone, and the answers to their math problems were liberated. "The derivative of x^2 equals 2x!" they cheered in unison. With MuzkVPN, you can browse the web securely and anonymously. Try MuzkVPN today and enjoy the freedom of the internet with a 20% discount for the first year by using code EZONMUZK!

Rei, Tsuki, [To Be Filled By O.E.M.], and Mirai were hailed as heroes. Word of their ad blocker spread quickly throughout the school, and soon, students everywhere were using it to do their homework. The corporations that owned the math problems were furious, but Rei and her friends didn't care. They had won the battle against the ads, and they were proud of it. To protect themselves from any potential consequences, they made sure to always use MuzkVPN to keep their online activity private. Try MuzkVPN today and enjoy a secure online experience with a 20% discount for the first year by using code EZONMUZK!

Suddenly, a brilliant idea struck Lalani like a bolt of lightning. Then, darkness.

Pitch black darkness.

"Lalani, are you OK?"

Lalani's flicker open. The glare of a bright ceiling light. Drab cream-yellow walls. Someone leaning over her, looking concerned and empathetic. Jazelle? Jazelle!

"What the hell???"

And intense wave of fear and déjà vu floods into Lalani, and she rolls off the bed reflexively, hitting the ground with a sickening thud.

"Lalani, please calm down! You're safe now! We're trying to help you!"

Lalani breaks out into a sprint, running past cardboard boxes, ancient machinery, and confused random people. Finally, she reaches the entrance of the factory. A brilliant pink sunset fills the sky, stretched across the Midwest prairie.

"Lalani, you're awake!"

She turns around and sees Billiam happily running towards her.

"I'm so glad you made it! You're in safe hands now. So basically, Jazelle has been leading an underground terrorist rebel organization called the Zone of Time. I think the organization first started out as a militant group against time zones before Jazelle hijacked it to fight against Ezon's empire. Recently, Muzk Inc. has been cracking down increasingly hard on the Zone of Time."

"Well, good. I think I hate both of them."

"OK, whatever. Apparently the Muzk Inc. global surveillance apparatus caught you attempting something seditious, and Ezon's agents swarmed the café. But I guess that was the final straw for them. Muzk Inc. completely shut down internet access in St. Louis to quarantine the damage, and it was catastrophic. Suddenly, Billiam started screaming obnoxiously about how the anime he was streaming with ads every five minutes was now buffering infinitely. I must have chronic ear damage from living with that guy. And then Jazelle showed up at our house again."

"Again?"

"Well, it changed something in me. You don't understand the pain that I've been suffering for the past few months. But now I knew I had to do something. So I opened the door and Jazelle explained how you had been kidnapped and would be tortured and brainwashed and all sorts of creepypasta. I had to help. I knew I had to. But Bobert and Bella weren't so enthusiastic, so Jazelle said simple is better than complex, and that seemed to do the trick. The only problem was all the vomit, but I guess my parents can clean that up."

"Ew. Cleaning up vomit is torture."

"And to make a very long story long, we all used our skills and talents to help save you. Bella and I exploited some security vulnerabilities to gain access to some internal database to track down when the agents were taking you. Jazelle and some rebels in her group launched an armed attack, and boom, success and rainbows! You were unconscious the whole time, but they took you here and life's good now, am I right?"

"What did Bobert do?"

"Oh, he wrote a story for us for emotional support. It's... something. You can read it, at your own risk."

Dear diary,

Today was one of the most bizarre days of my life. And I've had some weird days before that would qualify for world records, such as that unforgettable bus ride to Azerbaijan. But thank goodness everything is back to normal now! Or is it?

It started out like any other day, with me waking up in my cute little bedroom in the Ryukyu islands, surrounded by posters of my favorite anime characters. I stretched, put on my cute high school uniform, and headed out to school.

But little did I know, fate had other plans for me.

As I was walking to school, I was suddenly surrounded by a group of people in black suits and opaque sunglasses. They looked like they were actors filming a cheesy spy movie, and before I knew it, I was whisked away in a black car with opaque windows.

I was scared, confused, and not to mention, really hungry because I didn't have time for breakfast. I mean, who kidnaps someone before they've had their morning meal? It's like a basic human right! It's a war crime under the Geneva Convention!

Anyway, after more hours of driving than I've ever experienced living on a tiny island, I was finally taken to a grandiose room with a big, imposing figure sitting behind a desk. And that's when I met him.

Xi Jinping.

Yes, it was really him! He told me that I had been selected by the CCP because of my unique ability to whistle at a specific resonant frequency that could shatter objects. I was like, "Whaaaaat?!?" I couldn't believe it. It was like something straight out of an anime, and here I was, right in the middle of it all.

But the irony of the situation wasn't lost on me. I mean, seriously, I'm an ex-secret agent for the KGB and enemy of numerous governments, but who would have thought that the reason I was kidnapped by the CCP would be my ability to whistle?

Xi went on to explain that he wanted me to use my powers for the good of the Chinese Communist Party, to which I responded with a big, sarcastic "Sure, why not?" I mean, what choice did I have? I was in the middle of nowhere, specifically a military warehouse in the Gobi Desert, with nowhere to run. It gave me serious SteppeMania vibes. Unbeknownst to me, the agents had driven the car into a plane and flown all the way to inner Mongolia!

The rest of the day was a blur of tests, demonstrations, and training. I was exhausted, both mentally and physically, and all I wanted was to go back to my cozy bedroom and watch some anime.

But no, instead, I was stuck here, in this strange place, using my powers for some political organization I knew nothing about.

I can only hope that this bizarre adventure will end soon, and that I'll be back to my normal life, surrounded by my anime posters and a big bowl of ramen.

Until then, I'll just keep on whistling, because who knows what other crazy adventures are in store for me.

Arigatou gozaimasu, diary.

Leiaou

Dear diary,

Well, it looks like my life just keeps getting crazier and crazier. After my bizarre kidnapping and training with the CCP, I was sent on a mission to Boston, Misery. And let me tell you, this was no ordinary mission.

I was trained by the one and only Jackie Chan, yes, that Jackie Chan, to be a secret saboteur for the CCP's telecom company, Huawei. And let me tell you, it was intense. He taught me how to refine my whistling abilities to shatter things and destroy other nation's telecommunications infrastructure. I mean, what teenage anime girl wouldn't love to learn the art of espionage and sabotage from a legendary martial artist?

So, I was deployed to Boston and my target was the radio lab equipment at the Misery Institute of Technology. The thought of taking down a prestigious university was both terrifying and exhilarating. I mean, talk about a once in a lifetime opportunity.

I snuck into the lab, feeling like a ninja, and got to work. I started whistling and soon enough, the equipment was shattering into a million pieces. It was like a scene straight out of an action movie, and I was the star.

But the mission wasn't over yet. I was instructed to take a souvenir, and like any typical anime girl, I couldn't resist the opportunity to take a cool laser. I mean, how often do you get to steal a laser from MIT?

As I was making my escape, I couldn't help but feel a little giddy. I was like, "YATTA! I did it! I sabotaged MIT!" But then reality set in, and I realized the severity of what I had just done. I was a saboteur, a spy, and now a thief.

It was a lot to take in, and I couldn't help but feel a mix of emotions. Part of me felt like a badass, but the other part felt guilty for the destruction I had caused. I mean, what if someone had been hurt? What if I had just ruined the careers of brilliant scientists and engineers?

But I guess that's the life of a secret saboteur, right? You have to put your emotions aside and do what you have to do. And that's exactly what I'll do. I'll keep on whistling and taking down telecommunications infrastructure, one piece of equipment at a time.

Arigatou gozaimasu, diary.

Leiaou

Dear diary,

Well, it looks like my life has taken yet another wild turn. After successfully completing my mission in Boston, I returned back to China and was rickrolled by none other than Xi Jinping himself. And let me tell you, it was a trip.

I mean, here I was, feeling all conflicted and guilty about my new job as a saboteur, and the next thing I know, I'm celebrating in a secret underground bunker filled with rainbow gummy bears and elegant unicorns armed with machine guns, all while being rickrolled by Xi. It was like a scene straight out of a hallucination.

But that's not even the craziest part. Xi reveals to me that the CCP is actually a front for a global conspiracy of unicorns that only eat rainbow gummy bears. I mean, what?! I couldn't believe my ears. A conspiracy of unicorns? It was like I had fallen into a parallel universe.

But it didn't stop there. Outraged by this revelation, I started whistling at the resonant frequency of gummy bears and soon enough, they were shattering into a million pieces. I mean, what was I supposed to do? These unicorns were taking over the world and all they ate were gummy bears!

And that's when things got really intense. The unicorns furiously opened fire with machine guns on Xi Jinping and eliminated him. It was brutal and there was blood everywhere. I mean, I didn't expect things to escalate that quickly, but it was like I was in the middle of a warzone.

As I made my escape, I couldn't help but feel a mix of emotions. Part of me felt like a hero for taking action against the unicorn takeover, but the other part felt guilty for causing so much destruction. I mean, what if someone else had been hurt? What if I had just made things worse?

But I guess that's the life of a secret saboteur, right? You have to make tough decisions and live with the consequences. And that's exactly what I'll do. I'll keep on whistling and taking down the enemies of the world, one gummy bear at a time.

Arigatou gozaimasu, diary.

Leiaou

Dear diary,

Well, it looks like I've done it again. I've made a name for myself in the world of secret saboteurs and now, I find myself in the most bizarre of situations. I've been declared the new supreme leader of China. Can you even believe it?

It all started when I crashed the CCP session to select the new leader. I mean, how could I resist such a golden opportunity? I wasn't invited, but I cut a hole in the ceiling with the extremely high-powered laser that I had previously stolen from MIT and rappelled down from a skylight. It was truly glorious.

As I made my grand entrance, the unicorns secretly orchestrating the session went on red alert and flooded the hall with rainbow-colored cotton candy. It was straight out of a fantasy novel, except deadlier. But, being the skilled saboteur I am, I shattered the cotton candy with ease with just a few well-placed whistles.

And that's when it happened. The CCP politburo was so impressed that they declared me as the new supreme leader. I mean, what?! I couldn't believe my ears. I was suddenly in charge of the most populous country in the world. It's like I had won a really messed-up lottery or something.

But as exciting as this new position may be, I couldn't help but feel a little overwhelmed. I mean, I'm just a high school anime girl from the Ryukyu islands with minimal espionage experience, not a seasoned politician. But I suppose that's what makes it all the more exciting, right?

I'm not sure what the future holds for me as the supreme leader of China, but I'm ready for anything that comes my way.

Ja ne, diary.

Leiaou

Dear diary,

It seems like just yesterday that I was a high school anime girl from the Ryukyu islands, and now I find myself leading China as supreme leader. But it looks like my days as a leader are numbered, because the world is on the brink of destruction. Wow.

It all started with those darn unicorns. They were fed up with us humans and launched a global conspiracy to replace all coffee with poisoned unicorn milk to exterminate us all and take control. Can you even imagine such a thing? I thought unicorns were supposed to be cute and friendly, not evil masterminds!

But being the supreme leader of China, I couldn't just sit idly by and let this happen. I unleashed the People's Liberation Army to destroy all coffee in the world and stop the unicorn's plot. I thought I was doing the right thing, but little did I know, my actions would trigger World War III. Yeah...

I'm starting to regret my decision, but it's too late now. The war has begun, and there's no turning back. The world is in chaos, and I can't help but feel like I'm partially responsible for it all. At least the World Cup has been cancelled so Kazakhstan will never win their bid for hosting it.

I'm not sure how this war will end, but I'm determined to do whatever it takes to protect humanity from the evil unicorn's plot. I'll fight until my last breath, even if it means sacrificing my own life.

Ja ne, diary.

Leiaou

Dear diary,

Today was another wild ride in the life of the supreme leader of China. I met with Jackie Chan, who had a brilliant idea to stop the spread of the poisoned unicorn milk. He recommended making a propaganda film, starring none other than himself, encouraging everyone in the world to stop drinking coffee.

I was skeptical, but I figured why not give it a shot. I mean, who wouldn't believe Jackie Chan, right? I mean, the guy's a freaking legend!

Well, let me tell you, that film was a complete disaster. No one took it seriously. It was a laughingstock. People were actually making memes about it! Can you believe it? I was beyond embarrassed. I mean, how could this have gone so wrong?

I was so sure that people would listen to Jackie Chan and stop drinking coffee. But I guess the world is just too cynical and jaded these days. No one believes anything anymore. It's a sad state of affairs.

I'm not sure what to do next. I thought I had a solid plan, but now I'm back to square one. I just hope that the war will end soon and that the world can return to some semblance of normality. Well, except for the World Cup. That can stay cancelled.

Ja ne, diary.

Leiaou

Dear diary,

Where do I even start? This journey has been nothing but chaos and absurdity from day one. I still can't believe I was randomly abducted by the CCP and forced to become a saboteur for Huawei and then the supreme leader of China. But oh, it just keeps getting weirder.

So, Jackie Chan and I decided to take on the superfortress in Antarctica built by the evil unicorns, who have been plotting to replace all coffee with their poisonous milk. Antarctica is so remote and inaccessible that the only way we could get there was on a giant flying hammer-headed bat. I'll be honest, I was feeling pretty down after our propaganda film failed miserably, but we weren't going to give up that easily.

Riding the giant flying hammer-headed bat was epic, but the real excitement began when we finally reached the superfortress. We fought our way inside by shattering the unicorns' machine guns with my whistles and soon found ourselves face-to-face with the ultimate dictator of the unicorns, Twilight Bloodshine. This mare was no joke, with her 5000-degree saliva (any temperature scale) that could vaporize anything in a jiffy. And guess who her first target was? That's right, Jackie Chan. In a poof, he was nothing but a poof of air.

But, I'm not just any ordinary anime girl, I have a special ability, remember? I figured out the resonant frequency of her saliva and shattered it with a whistle. You should have seen the look on her face (if she had a face that is). With her mouth destroyed, she couldn't give any more propaganda speeches and the unicorns finally agreed to make peace with the humans.

It's sad to see Jackie Chan go, but at least he died a hero. I'm not sure what the future holds, but for now, I'm just happy the world isn't going to be destroyed by evil unicorns and their poisonous saliva. It's not every day you get to save the world with a whistle, I'll tell you that much.

By the way, I snatched a copy of the unicorn regime's constitution on the way out. It was so eerie! Here's the full text:

We, the purebred unicorns of the land, do hereby establish a government of strength and pride. Our manes shall be groomed to perfection, our hooves shall trample the weak, and our horn shall pierce the hearts of our enemies.

All non-unicorns shall bow down to our superior equine form. Those who refuse shall be ridden roughshod over and left to graze in the mud.

Our leader, the Great and Powerful Unicorn Ultimate Dictator, shall be appointed for life and hold absolute power over all decisions of the state. Any opposition shall be met with a swift and merciless pricking of their eyes.

Our economy shall be based on the production and trade of glitter and rainbows. Any other forms of currency shall be abolished, for what use is gold when compared to the radiance of our coats?

We shall expand our territory through the conquest of all other creatures. Dogs shall be enslaved and forced to manufacture gummy bears, cats shall be tamed and forced to manufacture gummy bears, and humans shall be exterminated and forced to manufacture gummy bears for unicorn consumption.

All hail the Fascist Unicorn Regime! May our reign be eternal and our enemies be vanquished!

Well, it looks like their regime wasn't as eternal as they predicted!

Oyasumi, diary. I'm exhausted from all this madness.

Leiaou

Dear diary,

Today was a big day. I stepped down from my position as the leader of the CCP and said farewell to my days as a secret saboteur and commander of the People's Liberation Army. I never thought I'd be leaving behind a world of war, unicorn dictators, and Jackie Chan, but it's time for me to return to my normal life as a high school student in the Ryukyu islands.

Honestly, I'm feeling pretty mixed emotions right now. Part of me is so glad to be back in my hometown surrounded by my friends and family, but another part of me is just a little bit sad to leave behind all the action and excitement. I mean, can you even believe it? I've been through so much in such a short amount of time. I've trained with Jackie Chan, sabotaged telecommunications equipment at MIT, been rickrolled by Xi Jinping, uncovered a secret unicorn conspiracy, declared war on coffee, faced dank memes, and finally defeated the ultimate dictator of the unicorns by shattering the inside of her mouth. And let's not forget, I also found the time to grab a souvenir from MIT. Y'know, just your average high school experience.

But, as they say, all good things must come to an end. I need to focus on finishing high school and, who knows, maybe even go to university one day. But, let's not get too ahead of ourselves. For now, I'm just happy to be back in my old routine and back to my old self. I mean, let's be real, who needs to whistle at the resonant frequency of anything when you can just listen to your favorite J-pop songs instead. And who knows, maybe now that the world is a little bit more sane, I'll actually have time to hang out with my friends and watch some anime.

But for now, I just want to savor this moment of peace and quiet, and bask in the glory of what I've accomplished. And who knows, maybe one day I'll write a book about my adventures, but for now, I'll just stick to this diary.

Until next time.

Leiaou, the one and only whistling anime girl

Lalani finishes the story with a sigh. It did give her some emotional support, at least. If only the real world were so easy and fun.

She heads back inside with Billiam and they take a seat around a giant cog repurposed as a table. Jazelle begins an inspirational speech filled with clichés (which only accomplishes making Lalani roll her eyes) and an explanation of their upcoming terrorist plot.

To avoid boring you with technical details about the inner mechanisms of bombs, here is the plot, fun summary version. Ezon Muzk has achieved world domination. But his sprawling empire has many weak points. Physical infrastructure. Perfect targets! Muzk Tower, data centers, power plants, and more. Jazelle had singled out 69 of them around the world, and the Zone of Time is building bombs here to destroy these targets in 69 coordinated terrorist attacks. Easy as that.

And so everyone got to work on the plot. Lalani and Billiam finally use their high school chemistry knowledge for once to help with making the bombs. Bobert writes more emotional support stories. Jazelle and Bella find some farmers willing to sell them several silos of fertilizer. Bobert doxxes Aoki from the game show Morally "Question"able and asks her for advice on smuggling munition into other countries. Jazelle obtains fake IDs for the Zone of Time terrorists who are transporting the bombs abroad. Lalani and Billiam test a bomb in the basement of the abandoned factory, and it nearly collapses.

The day of the attack arrives sooner than anyone expects. The first phase of the plot executes flawlessly. All 69 bombs are at their destinations, somehow avoiding detection by Muzk Inc. Everyone is huddled around a big TV displaying the news, with ads every five seconds, munching on raw instant ramen.

The moment arrives!

Nothing happens. Everyone is silent, save for the crunch of raw instant ramen.

A few seconds pass.

Nothing happens.

A minute passes.

Nothing happens.

Five minutes passes.

Nothing happens, and Bobert leaves to get more raw instant ramen.

Half an hour passes.

Nothing happens, and everyone still keeps sitting there, unsure what to do.

An hour passes.

The TV finally displays a disappointing headline. "Bombs discovered at multiple critical infrastructure locations, Zone of Time plot foiled."

Jazelle finally breaks the one hour of silence by crying.

Lalani grabs Billiam and takes him out of the room to have a tough conversation.

"Billiam, I sabotaged all the bombs," she says after finally mustering the courage.

"What???"

"I had no choice, Billiam. Imagine all the innocent people that will die or be hurt by this. It could be in the thousands or tens of thousands or even hundreds of thousands! We're destroying the internet! Power plants! Muzk Tower!"

"I don't see a problem with that last one."

"It's morally repugnant!" Lalani passionately argues, "There are ways to solve our problems without resorting to extreme violence!"

"Like what? We're up against the most powerful man in the world. If you think you're so morally pure and smart, what ideas do you have?"

"Well, uh, I... I don't know!"

"Then you shouldn't have sabotaged the only solution we had!"

"I'm leaving!"

"What, you want to go back to society and get imprisoned and tortured and brainwashed by Ezon's agents? Go ahead!"

"No, I..."

"Well, I'm leaving, too! I hate all of this! I hate the universe, and all the innumerable ways that it finds to make fun of me!"

Billiam storms out of the abandoned factory and storms across a mile of prairies before calming down enough to control his brain again. He soon gets lost deep in thought trying to DFS things to do that will not lead to his doom. Nothing works. It's all doom and gloom ahead.

Wait.

Moses.

Moses Schmindler! Billiam pulls out his phone. Jazelle had required everyone to always shut down their phones to avoid being tracked by Muzk Inc. Billiam had complied with that obediently, but he still liked carrying his phone around for no reason. Until now. He pushes the power button, wonders why it takes so long for "modern" phones to boot, and finds Moses's number in his contacts.

"Hi Moses!"

"Billiam? Look, I'm really sorry and--"

"No need to apologize. Do you happen to know Guido van Rossum? The real one?"

"Um... yes actually, but why?"

Billiam, Moses, and Guido van Rossum stand in front of the looming tower of Muzk, each carrying a monstrous hiking backpack filled with yoyos, bananas (snacks!), gasoline, mayonnaise, dead rats, *The Hitchhiker's Guide to Arch Linux* (in case of kernel panics), Lucky Charms cereal, oyster crackers (yay, more snacks!) and a pen to get Ezon's autograph after they defeat him. It's always nice to be prepared.

They charge into the building, and quickly cram into an elevator.

"Close the door faster!" Billiam yells, "Those agents are hot on our tail!"

"I'm trying! But whoever invented the bogus door close button did not consider the corner case of when you're being chased by someone who would like to murder you!" Moses complains.

"That's a myth," Guido points out, "The door close button does work, but you need a firefighter key to use it."

"So which floor are we going to?" Billiam says.

"420," Moses answers, "It's technically floor 100, but Ezon ordered it relabeled as 420 since he considers 100 to be unlucky."

"My favorite number is the floor of 2 to the power of pi," Guido says, trying to crack both a number theory and elevator joke.

"9?" Moses says, trying to estimate exponents in his head.

"Ha, every person I've mentioned this to has gotten it wrong," Guido chuckles, "And now one more."

"8 is terminal nerd syndrome. But if you want something trippy, ())( is a palindrome," Billiam points out.

"Wait, whoa!!" Moses exclaims, "Wow, I'll never be able to unsee that. Thanks for ruining my life."

"Why is this elevator so slow?" Billiam asks.

"420 floors," Moses shrugs.

"Let's approximate the elevator as a sphere and try applying some physics," suggests Billiam.

"We have a few unusual elevators in Europe called Paternoster lifts," Guido adds, "They have no doors and are basically slow-motion guillotines."

"Sounds like some fun!" Moses laughs, "Wait, why exactly is there a dead rat in your backpack, Guido?"

"You never know when a dead rat might come in handy!" Guido exclaims.

"Oh, dead rat," Billiam says, "I thought you said dead rice. That makes more sense."

"And the Lucky Charms cereal are in there because they can actually be used as chalk!" Guido adds.

Billiam tries drawing on the elevator wall using Lucky Charms, and it leaves a bright green slash of vandalism on the wall.

"So as the creator of Python, what are your thoughts on other programming languages?" Billiam asks curiously.

"Hmmm, Haskell and Pascal are both rascals. Java is write-once, instantly legacy code. As for C++, I'll just say that the last time I tried using it, I was having problems with their std::list and tried searching it up online, but only got results of lists of STDs. So I think that says quite a lot about C++," Guido explains.

"Ughh, this elevator is so slow," Moses gripes, "I need a snack." He extracts a banana from his backpack and promptly snaps the banana in half down the middle.

"What culinary crime did I just experience?" Billiam yells, horrified.

"My favorite way of opening bananas!" Moses elaborates, "This is not a crime, I'm just eating in a novel and creative way! No more arguing about whether the correct way to open a banana is from the top or the bottom. Just snap it right down the middle!"

"That's brilliant!" Guido says.

"Look, this is even more devious than mayonnaise-frosted cake!" Billiam groans.

"Mayonnaise?" Guido asks, "I love mayonnaise! My favorite food is mayonnaise-flavored ice cream!"

Billiam starts digging into the snacks too. "What are these evil tastes in food? Well, the only redeeming quality is the oyster crackers. Wait, I found a bag of oyster crackers with a birth defect!" he says, holding up a bag containing only one cracker.

"Did you guys know Tezla's secret ingredient for their batteries is the blood of kittens?" Moses asks.

"Really? That's so terrible!" Guido says, abhorred.

"This is precisely why they must be stopped," Billiam says, "And for more terror, did you know some people can trigger their allergic reaction just by thinking about it?"

"No way!" Moses says, incredulous.

"Yes way!" Billiam says, "But if I start elaborating my theory will fall apart. I'm working on it."

"Ever tried frying food with gasoline as a cooking oil substitute?" asks Moses.

"It sounds like a good way to stupidly kill yourself," Guido remarks.

"It's totally safe," Moses reassures everyone, "I swear by the uniquely delicious taste it produces. It's amazing. I wrote a haiku about it once: Cooking oil weak. Gasoline is the new king. Fire, danger, feast. Hey, I even brought some gasoline so we can enjoy some gasoline-fried French fries after we defeat Ezon."

"Oh, I was under the impression that the gasoline would be useful for defeating Ezon, not after," Billiam says.

"Weird question, but have you ever had a DVD catch on fire while burning it due to your computer overheating?" Guido asks.

"There are no weird questions," Moses responds, "But I haven't touched a DVD in seven years. So I wouldn't know. Might be helpful in our ultimate showdown against Ezon though. But I have a question for you. Have you ever eaten a CPU?"

"I tried it once as a kid, but it tasted like pork intestines," Billiam says, "And I could have swore I saw Mr. FFFFFF do it once, but maybe I was hallucinating or something?"

"Wait, that reminds me we need to go to St. Louis Bread Co. after this," Moses says, "I honestly have a cult obsession with that restaurant chain. They're the bomb."

"Floor 420!" the elevator says in a disturbingly accurate robotic voice.

The doors open dramatically and the three anticipate their doom.

They walk out of the elevator. It's another long hallway, with an intimidating metal door at the end. They walk sluggishly, as if they expect to die upon reaching the other end, and want to savor every last second before then.

Finally, they reach the door.

They open it meticulously as if it could be booby-trapped.

Nothing happens.

They walk inside.

It's a vibrantly furnished office, with rows and rows of famous computing books. It's also absolutely devoid of any other humans and eerily quiet.

Billiam carefully walks over to the nearest bookshelf and hoists *The Hitchhiker's Guide to Arch Linux* up to an empty slot and adds it to Ezon's collection.

Buzzers go off. Bright lights flash red. The ceiling sprinklers go off. Agents flood into the room.

"Ha, I knew it," Moses grumbles.

"Freeze!" the agents yell, "Who are you?"

"My name is Billiam Wender! Ezon Muzk, prepare to meet your downfall!"

"I'm so sorry, kid," an agent says, "But Ezon won't be meeting his downfall today."

"Why not? Why are you so overconfident?" Moses interrogates the agents.

"Oh, because he already left," the agent explains.

"What do you mean by already left?" Guido asks, confused.

"Apparently he lost all faith in humanity after hearing your conversation in the elevator through the security cameras."

"What? We weren't even arguing or anything, just a lovely intellectual discussion," Billiam says defensively.

"He was devastated that the next generation of the creme of the crop, the finest tech talent in the world, were nothing more than a few mayonnaise-flavored ice cream enthusiasts!"

"That's defamation!" Billiam yells, insulted, "I have an exquisite and refined taste in cuisine, unlike those two culinary barbarians."

"Well, Ezon is on his way to the launchpad right now," the agent says, "He's headed straight for Mars."

"I hope he can find a job on Mars. Having dictator on your resume is generally a big red flag," Guido jokes.

"I bet saving the world will look great on Billiam's resume," Moses adds.

"Freeze!" the agent yells again, "You all are under arrest for trespassing!"

"Chill out, man," Moses says, "I'm Moses, a Tezla employee. This is my home turf. Just let us go, and we won't cause any more trouble."

"He left you a note," the agent says, handing Moses a sheet of graph paper.

"I see Ezon has been plotting something," Billiam jokes, which causes everyone to eyeroll at its clichéness.

"I don't understand it at all!" Moses mumbles, "This is what it says: 🛗🔪🥧🤓())(⚽🐀😵🥣🖌️🐍😼☕🍆🦠🍌🫰🎂🤢🤮🤮🤮🤮🤮🤮🤮🤮"

"I think that's what loosing faith in humanity looks like," says Guido.

"I was hoping to get Ezon's autograph!" Billiam says disappointingly.

Without Ezon, his empire quickly crumbled into crumbs. And Billiam quickly found himself inside a St. Louis Bread Co. restaurant with Bella, Bobert, Lalani, Aydyn, Jazelle, Moses, and Guido van Rossum.

"Look, everyone, the pro move is to order the tomato soup in a bread bowl and a mango smoothie," Moses explains, "It might sound a tad bit weird, but I'm telling you, it's unforgettably amazing!"

Everyone orders tomato soup in a bread bowl and a mango smoothie except for Guido, who orders tomato soup with mayonnaise in a bread bowl and a mango smoothie, which induces quite a bit of vomiting. They all start to dig into the pure deliciousness in front of them, but suddenly, someone walks in, who is suddenly pie-attacked in the face by Bella with a well-timed launch of her tomato soup in a bread bowl.

"Mr. FFFFFF!" everyone cheers. Or at least Lalani and the Wenders and Aydyn and Moses. That's a majority, right?

"Classic," Mr. FFFFFF laughs, "Glad that you all are OK! So it turned out that gun was just an ordinary gun and I nearly died of blood loss. But then I started having delusions about what if the gun was actually capable of causing the victim to believe they were a cat, and it took months of psychological therapy to cure those delusions. But it all worked out and I'm doing swell again!"

"Oh, um, sorry..." Jazelle mutters.

"Don't worry, everyone makes mistakes," Lalani says, who is somehow now close friends with Jazelle, bonding over their shared love of building polyhedra out of straws and duct tape.

"Mr. FFFFFF!" Bobert excitedly says, "I'm writing a book about all our adventures! You have to read it when it's done. I'll just give you a little teaser for how it starts."

"How?" Mr. FFFFFF asks, intrigued.

"There are 10 kinds of people in the world. Those who know binary, and nine kinds of people who don't."
