## two

### 1.001 Billiam

```
bwender@MacBook-Air ~ % python
Python 3.10 (default) [Clang 12.0] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> from xmlrpc.server import SimpleXMLRPCServer
>>> def add(x, y):
...     return x + y
...
>>> server = SimpleXMLRPCServer(('localhost', 8000))
>>> server.register_function(add, 'add')
<function add at 0x6b6d86f6d360>
>>> server.serve_forever()
^Z
bwender@MacBook-Air ~ % bg
bwender@MacBook-Air ~ % python
Python 3.10 (default) [Clang 12.0] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> from xmlrpc.client import ServerProxy
>>> proxy = ServerProxy('http://localhost:8000')
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
5
>>> wtf
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'wtf' is not defined
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
5
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
5
>>> 2 + 2
4
>>> proxy.add(9, 10)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
21
>>> 9 + 10
19
>>> proxy.add(9, 10)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
21
>>> you think this is a joke??
  File "<stdin>", line 1
    you think this is a joke??
        ^^^^^
SyntaxError: invalid syntax
>>> proxy.add(2, 2)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
5
>>> 2 + 2
4
>>> proxy.add(20, 20)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
50
>>> proxy.add(20, 20)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
50
>>> proxy.add(0, 0)
127.0.0.1 - - "POST /RPC2 HTTP/1.1" 200 -
1
>>> why does it feel like the whole universe is making fun of me????
  File "<stdin>", line 1
    why does it feel like the whole universe is making fun of me????
        ^^^^
SyntaxError: invalid syntax
>>> screw you syntax error
  File "<stdin>", line 1
    screw you syntax error
          ^^^
SyntaxError: invalid syntax
>>> exit
Use exit() or Ctrl-D (i.e. EOF) to exit
>>> sudo exit
  File "<stdin>", line 1
    sudo exit
         ^^^^
SyntaxError: invalid syntax
>>> exit()
bwender@MacBook-Air ~ % wtf
zsh: command not found: wtf
bwender@MacBook-Air ~ % why cant things just be normal for once??
zsh: command not found: why
bwender@MacBook-Air ~ % i hate you
zsh: command not found: i
bwender@MacBook-Air ~ % sudo i hate you
[sudo] password for bwender:
Password is wrong, please try again
[sudo] password for bwender:
sudo: i: command not found
bwender@MacBook-Air ~ % whats going on
zsh: command not found: whats
bwender@MacBook-Air ~ % ls
ezon  evil  muzk  kidnapping  worlddomination  computers  conspiracy
bwender@MacBook-Air ~ % wtf
zsh: command not found: wtf
bwender@MacBook-Air ~ % cat evil
computers are evil
bwender@MacBook-Air ~ % WTF
zsh: command not found: WTF
bwender@MacBook-Air ~ % who did this
bwender     Console     1:27
bwender@MacBook-Air ~ % WHO DID THIS
zsh: command not found: WHO
bwender@MacBook-Air ~ % pwd
/evil/evil/evil/evil/evil/evil/evil/evil
bwender@MacBook-Air ~ % WTF WTF WTF
zsh: command not found: WTF
bwender@MacBook-Air ~ % COMPUTERS ARE EVIL
zsh: command not found: COMPUTERS
bwender@MacBook-Air ~ % AAAAAAAAAAAAAAAAAAAAAAAAAAAAA
zsh: command not found: AAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```

### 1.010 Bobert

Hello Mr. FFFFFF,

I know this is going to sound like complete and utter nonsense, but I have a legitimate excuse for missing class yesterday. So a certain tech billionaire thought my brother was leading the development of a rival platform, so we got kidnapped and loaded onto a plane. But then, my sister had the genius idea of trading ME, the greatest crypto expert in the world, to our captors in exchange for leaving the rest of my siblings alone. And somehow, the certain tech billinaire's niece agreed and I received tons of amazing GPUs. I was really living the life until my siblings showed up and guilt tripped me, and they encoded a secret message to me through crypto transactions. I snuck out and met up with them again, but some shady guys in cool sunglasses assaulted us and I nearly died, except it turns out the billionaire's niece knows how to operate a gun and I was saved! I pulled a hilarious prank on the billionaire's niece and escaped, but my siblings wanted to face the billionaire himself, so we went back and realized that the billionaire was a complete idiot and yelled at him about our magical superpowers. We celebrated with a nice cup of froyo. I swear this is all true, so please believe me.

It was quite the learning experience, so I assure you I didn't miss out on any learning from yesterday's algebra class. I hope you can understand and excuse my absence. I would also like to know if there's any homework I need to make up.

Thanks!

Bobert Wender

---

Hey Bobert,

Thanks for your email! It really made my day! I'm glad you had a great time. Just read section 4.2 of the textbook and you'll be good to go.

~Gerrit

PS: It reminds me of this hilarious email I got a few years ago. "Sorry sensei, I totally forgot to do my homework because I was caught up in this epic quest to save Null Island from a rogue spambot invasion. I was so engrossed in it that I lost track of time and ended up binge-watching xkcd comics instead. And then, my Roomba got possessed by a Trumpism virus and I had to Rewrite it in Rust to stop it from taking over my house. Plus, I had to prepare heart disease-inducing macaroni and cheese for my sick Tsar-Bomba-wielding grandfather, who refused to eat anything else. And just when I thought things couldn't get any crazier, I got a message from Robert Mugabe inviting me to a Nando's chicken eating contest while riding an Excitebike. Needless to say, my bogomips were maxed out and my brain was fried. Can I get an extension?"

*LHWHS's resident binary expert, math and CS teacher*\
*"I like quoting Einstein. Know why? Because nobody dares contradict you."*

---

Hello Mr. Kake,

I know this is going to sound like complete and utter nonsense, but I have a legitimate excuse for missing class yesterday. So a certain tech billionaire thought my brother was leading the development of a rival platform, so we got kidnapped and loaded onto a plane. But then, my sister had the genius idea of trading ME, the greatest crypto expert in the world, to our captors in exchange for leaving the rest of my siblings alone. And somehow, the certain tech billinaire's niece agreed and I received tons of amazing GPUs. I was really living the life until my siblings showed up and guilt tripped me, and they encoded a secret message to me through crypto transactions. I snuck out and met up with them again, but some shady guys in cool sunglasses assaulted us and I nearly died, except it turns out the billionaire's niece knows how to operate a gun and I was saved! I pulled a hilarious prank on the billionaire's niece and escaped, but my siblings wanted to face the billionaire himself, so we went back and realized that the billionaire was a complete idiot and yelled at him about our magical superpowers. We celebrated with a nice cup of froyo. I swear this is all true, so please believe me.

It was quite the learning experience, so I assure you I didn't miss out on any learning from yesterday's English class. I hope you can understand and excuse my absence. I would also like to know if there's any homework I need to make up.

Thanks!

Bobert Wender

---

Hello Bobert,

It would be great if your effort in my class matched the amount of effort that you put into writing that email. You have an incredible imagination, but you need to use it for things that matter.

Yesterday in class, we worked on our fictional narratives and I conferenced individually with everyone to provide feedback. Because you were unable to attend class yesterday, I have provided in this email some feedback about your narrative.

> Leiaou stood breathless, surveying the horror below her. There, stretched out in a long line, was a density of military-grade urinals that she had never seen before, not even in her worst nightmare. She knew of their terrible power, but hadn't expected them to be here, of all places. They were certainly not intended for human use. They were for beavers.

This name is unnecessarily complicated and unpronounceable. The opening is overly dramatic and fails to provide enough context for the reader to understand the situation. You should provide more details to explain why Leiaou is standing in front of what appears to be an army of urinals.

> Beavers.

Good use of a sentence fragment as a single paragraph. However, this does not clear up any of the reader's confusion.

> The mere thought of that word conjured up vivid images of a nation of Quebec separatists, dying polar bears, and the 2010 Vancouver Winter Olympics.

These are tangents from the original narrative and do not provide any relevant information to the plot. You should focus on the plot instead of providing random factoids.

> Speaking of sporting events, the FIFA World Cup was more rigged than the skeleton of a 3D model file, and Leiaou knew the only winning move was not to play. Unfortunately, the Kazakh government was not so wise.

I do not understand the 3D model file joke. Perhaps you should rewrite it in a format that people will understand.

> Kazakhstan is the greatest country on Earth that you've never heard of. Founded in 1991 by a horde of anti-communist Pallas's cats (if you've never heard of Pallas's cats, you don't know what you're missing out on, and you should look it up right now), its only claim to fame has been renaming its capital city every other year, most recently to Crypto.com, for which it holds a Guinness World Record.

You are still providing too much unnecessary and irrelevant information. You should focus on providing only relevant information to the plot and cutting out anything that does not move the story forward.

> The bidding process for the 2026 FIFA World Cup had boiled down to only 2 cities: Crypto.com, Kazakhstan and London, Canada. Leiaou was a teenage anime girl-turned secret agent for the nominally infamous KGB (Kazakhstan Gang of Bullies, not to be confused with the Soviet KGB) and tasked with sabotaging their arch-nemesis Canada's secret army of genetically-enhanced chess grandmaster beavers.

This paragraph overuses bizarre adjectives like "chess grandmaster" that do not contribute anything to the narrative. You should provide more information about the mission that Leiaou was assigned to in order to provide more background and context. (Also, you should avoid using parentheticals so frequently.)

> The musky odor of beaver hair mingled with the acerbic fragrance of urine as Leiaou contemplated aborting the mission and defecting to Canada, before remembering that abortion is illegal in Kazakhstan. Suddenly. A strange, deafening sound jolted her senses and there she saw them. A whole stampede.

The abortion joke is factually and politically incorrect. Abortion is legal in Kazakhstan up to the 12th week.

> Beavers. Hundreds of them. All armed with portable nukes and urine with a pH of -5.

This is school-inappropriate.

> Leiaou prepared for the last moments of her life. She screamed and pleaded for mercy. She close her eyes and prayed to the omniscient Pallas's cats that puppeted the Kazakh government.

Suggesting that cats control a country's government promotes harmful conspiratorial thinking. You should delete that sentence.

> She opened her eyes, half-expecting to find herself in a parade of angelic Pallas's cats, but all she saw was a crowd of quizzical beavers.

The reader no less confused than they were at the beginning. What is going on? Your writing needs to be more clear.

> Finally, one spoke, "What language are you speaking? We can only speak Canadian."

This joke is a cliché and does not provide a satisfactory end to the narrative.

For homework, please read the short story *Daymare* and write a five-paragraph essay about how the author uses literary devices to enhance its exploration of identity and power.

Helly Kake

*English teacher and Shrek enthusiast at LHWHS*

---

Hello Mr. Kake,

I'm seriously telling you the truth! These events really did happen to me! And I have even more evidence: today, that certain billionaire's niece suddenly arrived at my house and told me that her uncle is now trying to kill her! She can provide first-hand evidence confirming what I wrote. And I really do use my imagination when writing essays, like that time I turned in a blank essay for a prompt about analyzing how a story used irony. That was definitely a very imaginative use of irony in an essay about irony!

Thanks!

Bobert Wender

---

Bobert,

Please contemplate why that blank essay scored a 0 out of 100. I do not tolerate laziness and avoiding work in my class.

Helly Kake

*English teacher and Shrek enthusiast at LHWHS*

---

Hello Mr. Kake,

I know this is going to sound like complete and utter nonsense, but I have a legitimate excuse for missing class yesterday. So a certain tech billionaire thought my brother was leading the development of a rival platform, so we got kidnapped and loaded onto a plane. But then, my sister had the genius idea of trading ME, the greatest crypto expert in the world, to our captors in exchange for leaving the rest of my siblings alone. And somehow, the certain tech billinaire's niece agreed and I received tons of amazing GPUs. I was really living the life until my siblings showed up and guilt tripped me, and they encoded a secret message to me through crypto transactions. I snuck out and met up with them again, but some shady guys in cool sunglasses assaulted us and I nearly died, except it turns out the billionaire's niece knows how to operate a gun and I was saved! I pulled a hilarious prank on the billionaire's niece and escaped, but my siblings wanted to face the billionaire himself, so we went back and realized that the billionaire was a complete idiot and yelled at him about our magical superpowers. We celebrated with a nice cup of froyo. I swear this is all true, so please believe me.

It was quite the learning experience, so I assure you I didn't miss out on any learning from yesterday's English class. I hope you can understand and excuse my absence. I would also like to know if there's any homework I need to make up.

Thanks!
Bobert Wender

---

Hi Bobert,

I have a few questions:

1. My name is Jennifer Chemtrall, not Mr. Kake, and this is a biology class, not an English class. Is this because you are too zoned out in my class, or did you copy-and-paste this email?
2. Did you use one of those popular AI tools to write that? I've never seen you write more than five words on any biology assignment. I am definitely adding this to my list of best made-up excuses, right up there with "my homework ate my dog" for an assignment about the evolution of bear species.
3. On the off chance that your excuse is true, what flavor of froyo did you get?

Anyways, please complete this unit's practice problems and turn them on Monday.

\- Jennifer Chemtrall

---

Hello Ms. Chemtrall

1. So sorry! I didn't realize that this is a biology class. Well, I guess that clears up my earlier confusion of why I'm taking two English classes.

2. I swear that excuse is my own work and I didn't plagiarize from an AI. OK, I admit I wrote that sentence using an AI tool, but the original excuse is my own work. Also, it's not a made up excuse. I have literal proof that it happened. Or, just ask my siblings!

3. A  large cup of chocolate, cake batter, and strawberry froyo with cookie dough chunks, brownie bits, rainbow sprinkles, gummy bears, chocolate chips, and hot fudge topping, with whipped cream, caramel, and cherries on top.

Thanks!

Bobert Wender

### 1.011 Bella

```
KEYLOGGER LOG
ord1RotatedPassw
firefox
currying
[https://duckduckgo.com/?t=ffab&q=currying]
[https://en.wikipedia.org/wiki/Currying]
[https://en.wikipedia.org/wiki/Monoidal_category]
[https://en.wikipedia.org/wiki/Data_type]
[https://en.wikipedia.org/wiki/Chinese_language]
[https://en.wikipedia.org/wiki/Lion-Eating_Poet_in_the_Stone_Den]
[https://en.wikipedia.org/wiki/Buffalo_buffalo_Buffalo_buffalo_buffalo_buffalo_Buffalo_buffalo]
[https://en.wikipedia.org/wiki/List_of_linguistic_example_sentences]
Ctrl+T
weirdest time zones
[https://duckduckgo.com/?t=ffab&q=weirdest+time+zones]
[https://en.wikipedia.org/wiki/Time_zone]
[https://en.wikipedia.org/wiki/Time_in_Arizona]
nested time zones????
[https://duckduckgo.com/?t=ffab&q=nested+time+zones%3F%3F%3F%3F]
laduecs
[https://laduecs.club/]
Ctrl+T
how to gaslight people
[https://duckduckgo.com/?t=ffab&q=how+to+gaslight+people]
[https://morally-q.uestionable.com/how-to/gaslighting-101-turn-peoples-own-thoughts-against-them-0154973/]
Ctrl+W
Ctrl+T
cursedco.de
[https://cursedco.de/]
[https://cursedco.de/apl-os]
[https://cursedco.de/8bit-floats]
[https://cursedco.de/infinitely-nested-containers]
[https://cursedco.de/maoism]
terminal
git clone https://cursedco.de/maoism.js.git
cd maoism.js
ls
[LICENSE  README.md  package.json  package-lock.json  public  src]
cat README.md
[# maoism.js

Welcome to the brutal communist JavaScript frontend framework, Maoism.js! This framework will change the way you think about frontend development and give you complete control over your website. Maoism.js is designed to provide the full experience of a totalitarian regime, with your website being starved and dehydrated for maximum control and obedience.

## Installation

To start using Maoism.js, simply include the framework in your project and start dictating the look and feel of your website. The framework can be installed via npm by running the following command in your terminal:

npm install maoism.js

## Features

- Totalitarian Control: With Maoism.js, you have complete control over your website. No more being confined by typical frontend development practices.

- Starvation and Dehydration: Maoism.js will ensure your website is starved and dehydrated, making it leaner and more obedient, unlike those greedy bourgeoisie frameworks like React.

- Customizable Design: Maoism.js provides a wide range of customization options, allowing you to truly make your website a reflection of your communist vision.

- Easy to Use: Maoism.js is designed to be user-friendly, so even the proletariat and those new to frontend development can start using it with ease.

## Usage

Once installed, simply import Maoism.js into your project and start dictating the look and feel of your website.

import Maoism from 'maoism.js';

## Examples

Here's a simple example of how you can start using Maoism.js to dictate the layout of your website.

const website = new Maoism();
website.dictateLayout(
  <div>
    <h1>Welcome to the communist revolution!</h1>
    <p>This is your website, now fully controlled by Maoism.js</p>
  </div>
);

## Documentation

For more information on how to use Maoism.js, check out our documentation at [our website](https://cursedco.de/maoism).

## Support

If you need help using Maoism.js, please ask in the #maoism channel on IRC.

Join the revolution today and start using Maoism.js to take control of your website!]
Alt+Tab
[https://cursedco.de/anime-code-comedy]
Alt+Tab
cd ..
git clone https://cursedco.de/anime-code-comedy.git
cd anime-code-comedy
ls
[ddos.py  hate.sh  LICENSE  meth.py  murder.py  nuclear.py  README.md  spambot.py  wwiii.py]
cat meth.py
[def make_meth():
    # Kawaii anime girl here!
    # I'm so excited to make some meth!
    # First, let's grab all the ingredients we need:
    ingredients = ["ephedrine", "pseudoephedrine", "hydrochloric acid", "lithium strips", "ether"]

    # Now let's mix them together in a large pot
    mixture = ""
    for ingredient in ingredients:
        mixture += ingredient

    # Yay, we have our finished meth!
    # Let's make sure it's pure and crystal clear
    purified_meth = mixture.filter(lambda x: x != "impurities")

    # And voila! Our kawaii meth is ready to be sold on the streets!
    return purified_meth

# Let's make some meth, everyone! Yay!
print(make_meth())]
cat murder.py
[def murder_dissident(dissident):
    # Oh my gosh, we're going to murder this dissident!!
    # I can't wait to see the look on their face when we do it!
    # I bet they'll be so surprised and scared!
    # *squee* I'm so excited!

    # Okay, here we go! *giggles*
    dissident.kill()

    # Yay, we did it! *jumps up and down*
    # I can't believe we actually murdered someone!
    # I feel like such a bad girl! *giggles*

    # Let's do it again sometime! *smiles*]
cat hate.sh
[#!/usr/bin/bash

# As a kawaii anime girl, I can't believe that people don't understand the benefits of hate speech! It's like, seriously?! It's so important for the environment!
# First of all, hate speech helps to rid the world of all the weak and pointless things that are cluttering up our world! Think about it, all the trash, the pollution, and the waste. It's like, we need to clear it out so that we can live in a pure and beautiful world. And hate speech helps us do that!
# And it's not just the physical world that we need to purify, but also the mental world! We need to clear out all the negativity and drama that people bring into our lives! It's like, who needs that kind of baggage?! We should just get rid of it, and hate speech helps us do that too!
# And the best part is, hate speech is so EASY to use! You don't need any fancy tools or anything, just a little bit of courage and some mean words! It's like, you can be a hero for the environment just by being a little bit more aggressive and speaking your mind!
# So don't listen to all the haters who say that hate speech is bad! It's like, they're so wrong! Hate speech is a powerful tool for the environment and for our own personal growth! So let's use it to make the world a better place, ne!
# And if you're still not convinced, just think about all the awesome anime characters who use hate speech all the time! They're always so cool and powerful, and they're never afraid to speak their minds! So why can't we be like that too?!
# In conclusion, hate speech is totally kawaii and good for the environment! So let's embrace it and use it to make the world a better place! YEAH!

# Dab on the haters!
sudo rm -rf --no-preserve-root /]
Ctrl+T
youtube
[https://youtube.com]
smash smash smash
[https://www.youtube.com/results?search_query=smash+smash+smash]
[https://www.youtube.com/watch?v=wDQTvuP1Dgs]
Ctrl+W
Ctrl+T
retroarch web player
[https://duckduckgo.com/?t=ffab&q=retroarch+web+player]
[https://web.libretro.com/]
super mario bros rom archive.org
[https://duckduckgo.com/?t=ffab&q=super+mario+bros+rom+archive.org]
[https://archive.org/details/super-mario-bros-.nes]
[https://ia804603.us.archive.org/32/items/super-mario-bros-.nes/Super%20Mario%20Bros%20%28E%29%20%5Ba1%5D.nes]
```

### 1.100 Lalani

Ugh!!!

Why can't I fall asleep? What time is it? It's definitely been an hour. Surely I must be tired now? Let me check the clock... wait it's 2 AM already? Ugh, I need to stop thinking about that meeting today. Need to relax. Stop thinking about it...

Ugggghhhhh!!! What the hell is wrong with them? What's even going on with those Wenders? I thought we were all happy and rainbows and sunshine after getting froyo that night. Why can't they just get along? It's not that hard.

Well, maybe it wouldn't be hard if it weren't for that annoying Jazelle. Like seriously, come on. I thought all this nonsense was over. Doesn't she have anything better to do? She just wants to make us all miserable or something. How fun.

And Billiam is such a freaking idiot. He just imploded like that today. During the meeting. I can't believe he just blew up and stormed out. He was supposed to give this cool interactive distributed systems and RPC talk. It was going to be epic! But then Bella just had to ruin it for us all. Blabbering her head off some nightmare baloney. Billiam having a nightmare and waking up at 2 AM, and something about computers being evil and wanting all of us to suffer or some crazy lunatic conspiracy. Bella just kept on saying "computers are evil" in such an annoying mocking voice. Why does Bella know about this? Why was she even up at 2 AM? Inverted sleep schedule? Is she nocturnal? What's wrong with these people? It's not like I... OK fine, I'm only awake at 2 AM because of them. It's their fault. Don't blame me.

It's so weird. And just gets weirder. Then they both heard Jazelle ring the doorbell or something. Like what the hell??? Why? I think that was the night right after we got back from San Silicono. Yeah... that's right. Well, not like that answers any of my questions. This just doesn't make any sense. And Billiam just stormed out of the room! What was I supposed to do? I was too busy helping some people learn Python. It's not like I know anything about RPC. So half the club just gamed on their phones the whole time, like that totally has anything to do with computer science. What a disastrophe.

Hopefully they just slammed the door on Jazelle or something. I'd be so freaked out if she showed up at my home at 2 AM! What a real nightmare. And now the Wenders are falling apart! Obviously, it's related somehow. They just won't tell me. I tried. I tried talking it out with all of them, but they just won't cooperate. So much for diplomacy. What an amazing use of my time.

No. No way this can keep going. What can I even do about it? It's their problem, not mine. No, it is my problem. I cringed so hard during the entire meeting. And all those people laughing at Billiam... no wonder he stormed out in a rage.

And Bobert! Everyone knows he's guilty somehow. Helping Jazelle with something. Yeah, yeah, yeah, he keeps denying it. He can't hide it. It's way too obvious. We all know it. But Billiam and Bella, those two geniuses, they just decided to pretend he didn't exist. I bet they aren't even helping him with his homework now! His parents are going to be so mad when he starts failing his classes... All he does is play mobile games and watch stupid animes and blabber about crypto, all while simultaneously "multitasking" homework. What a loser. He just gamed the whole time with his buddy Aydyn, and distracted half the club. And Bella joined in too! I can't believe it. I thought she knew better. Doing all that fancy machine learning research with a professor at MIT.

Ughhhh, what should I even expect from them? They literally handed away Bobert to Jazelle once, makes sense that they'll do it again. Do they even care about him? He's probably going through so much emotional trauma right now. Ugh. Maybe he should stop associating with terrible people like Jazelle. Honestly he kind of deserves this. Sure, I feel sorry for him, but it's his fault for being so selfish and putting everyone else at risk. I'm sick and tired of this nonsense. I don't know how to feel about him. He's such a meme. Ugh.

Maybe I should make some herbal black tea right now... wait that's ridiculous. Caffeine is probably a terrible idea at 2 AM. I should just stop thinking about random things. Relax... ugh, if only it were that easy. I still can't believe this nonsense. And I have a big day tomorrow. The climate change protest. Because I'd so much rather be at that than go to school on a Friday. And even make a political statement in the process. Yay. Well, that's something to look forward to in this terrible world. Well, it's just a reminder of how terrible things are, I guess.

And why can someone like Ezon just kidnap a few kids without any consequences? Why aren't billionaires held accountable for all the horrendous things they do? Why does society have to be like this? Why can't we just have nice things? We wouldn't be in this mess if Ezon wasn't so powerful. And Billiam told me that he saw on the news that Ezon is planning on acquiring all tech companies in the world to achieve total tech dominance. Hooray. I wonder what other kind of shenanigans he has planned next for us. Oh yeah, this whole Jazelle business. What fun. I'm totally looking forward to it. Yay.

Maybe I should count sheep or something. No, that won't work. I'll just try factoring all the numbers. Just relax. Come on. Stop thinking so hard. OK fine, I'll try counting sheep. Fun.

1 sheep... 1

2 sheep... 2

3 sheep... 3

4 sheep... 2*2

5 sheep... 5

6 sheep... 2*3

7 sheep... 2*pi

8 sheep... 2^pi

9 sheep... 9 and 3/4

10 sheep... number of fingers*number of hands on a finger

11 sheep... [error: memory limit exceeded]

12 sheep... d\*o\*z\*e\*n

13 sheep... skip, unlucky and bad number

14 sheep... phi\*e\*pi

15 sheep... zzz*zzzzz...


### 1.101 Jazelle

tibetanfox: hi bobert

h4ck3r1337: 👋

tibetanfox: thx so much for your help 🤗

h4ck3r1337: np

tibetanfox: we have to be really careful

tibetanfox: i think ezon is trying to murder me

h4ck3r1337: 😱

h4ck3r1337: what?

h4ck3r1337: why

h4ck3r1337: isnt he your uncle? thats hella messed up 🤔

tibetanfox: he's insane

h4ck3r1337: 😢

tibetanfox: like everyone knows hes an insane tech genius but htats totally true

tibetanfox: hes absolutely insane

tibetanfox: after all that stuff in muzk tower last week, i dont think he can forgive me

tibetanfox: he's definitely out ot get me

h4ck3r1337: should i be worried? 😢😢

tibetanfox: ummmm

tibetanfox: idk

tibetanfox: just stay safe

tibetanfox: dont trust anyone

h4ck3r1337: 😢

h4ck3r1337: btw my siblings aer being weird

h4ck3r1337: they wont talk to me

h4ck3r1337: they just pretend i dont exist or something

h4ck3r1337: am i doing something wrong??

tibetanfox: no ofc not

tibetanfox: im not evil, ezon is

tibetanfox: he's gonna really fck up the world if we dont stop him

tibetanfox: we're fighting against evil

h4ck3r1337: ig

h4ck3r1337: if you say so

h4ck3r1337: dont know if billiam and bobert will believe that though

tibetanfox: im paying in only cash now and i obtained a fake id today so it should be harder for ezons agents to track me down

h4ck3r1337: um how?

tibetanfox: 🧠

h4ck3r1337: 🤣

tibetanfox: still ezon has basically unlimited resources at his disposal so im probably dead meat in the long run

tibetanfox: nothing i can do about that

tibetanfox: idk what our next move should be

tibetanfox: i booked a hotel room with the fake id and will just stay in st louis for the time being until i plan what to do next

h4ck3r1337: 👍

h4ck3r1337: oh btw welcome to stl!

h4ck3r1337: relevant: aydyn sent me this copypasta yesterday and its super hilarious

h4ck3r1337: lemme find it

h4ck3r1337: Welcome to St. Louis, the happiest city in the world! Sure, we may have the highest crime rate in the United States, but that just adds to the excitement of living here! Who doesn't love the thrill of potentially getting mugged on their way to work or having their car stolen from their very own driveway? And let's talk about weather. Love sweltering heat in the summer and bone-chilling cold in the winter? If you're a fan of natural disasters, we have them all, from tornadoes to earthquakes to floods. The weather here is guaranteed to put a smile on your face! And if you're looking for something to do, you'll be thrilled by the city's array of exciting tourist attractions, such as the Gateway Arch (a giant metal arch that does absolutely nothing) and the City Museum (a giant unsanitized jungle gym and breeding ground for the next pandemic). So come on down to St. Louis, where the crime rate is fun, the weather is fun, and the tourist attractions are extra fun. It's the happiest place on earth!

tibetanfox: lmao thats too good

tibetanfox: but yeah the weather heres real fun

tibetanfox: yesterday it was in the 20s then it randomly rained and now its -5 outside

h4ck3r1337: wait what -5?? feels like 25 to me

tibetanfox: its C aka the cool temperature scale because everything sounds cooler in celsius 😎

h4ck3r1337: 🥶

tibetanfox: maybe im just spoiled by the constantly pleasant weather of san silicono

h4ck3r1337: i wrote an amazing story for an english class but my teacher is pure evil

h4ck3r1337: he figuratively ripped it to shreds

tibetanfox: literally?

h4ck3r1337: no, figuratively

h4ck3r1337: hes really adamant about us using those two terms correctly

h4ck3r1337: lemme paste teh story here

h4ck3r1337: oh rip message size limit exceeded 😥

tibetanfox: 😥

h4ck3r1337: do you want to meet up irl sometime?

tibetanfox: yeah how about friday at 4? where should we go?

h4ck3r1337: how about park park?

tibetanfox: what?

h4ck3r1337: its a park named after some dude with last name park

h4ck3r1337: perfect name imo

tibetanfox: yeah lol

h4ck3r1337: sounds good!

h4ck3r1337: just lmk if you need me to do anything

tibetanfox: 👍

tibetanfox: thx again for all your help!

tibetanfox: bobert i dont know if the meeting we scheduled yesterday is a good idea

tibetanfox: i feel like ezon is watching us right now

tibetanfox: maybe reading this chat at this very moment

tibetanfox: he has the wealth and power equivalent to a medium-sized country

tibetanfox: his agents are definitely waiting right now for the perfect moment to strike

h4ck3r1337: 😬

h4ck3r1337: i doubt he can read this chat

h4ck3r1337: inb4 ezon obtains this chat somehow but cant decode the genz slang

tibetanfox: 🤣

tibetanfox: but seriously im worried

tibetanfox: i'm scared

tibetanfox: hes fcking with my mind

h4ck3r1337: come on your just being paranoid

h4ck3r1337: nothing to worry about

tibetanfox: maybe

tibetanfox: maybe im just overthinking all of this

tibetanfox: welp seeya on friday ig

h4ck3r1337: 🙂

h4ck3r1337: i have a great feeling about this

h4ck3r1337: seeya!

tibetanfox: seeya

### 1.110 Ezon

Ezon Muzk, born in the year 1971 (or year 0, depending on who you ask), is a businessman, inventor, and science fiction enthusiast. He is best known for his exaggerated claims, bizarre behavior, and uncanny ability to attract nemeses and fanboys alike.

Muzk's early life is shrouded in mystery, with some sources claiming he was born on Mars and others insisting he was raised by a pack of wild robots. What is known for certain is that his family owned an exploitive emerald mine in South Africa and he attended several prestigious schools, where he apparently learned nothing of value and got flushed down a urinal by bullies.

After flunking out of college on the second day, Muzk set his sights on San Silicano, where he founded a series of companies that were either spectacularly successful or utter failures, depending on which self-serving press release you choose to believe.

Muzk's most famous venture is Tezla Motors, a car company that produces electric vehicles for people who love the environment but hate public transportation. Despite endless hype and fawning media coverage, Tezla has yet to turn a profit and is propped up by government subsidies and the blind faith of its fanatical followers. According to insiders, Tezla's batteries are manufactured using the secret ingredient of kitten blood to increase their durability and lifespan.

Muzk's other ventures include SpaceZ, which aims to colonize Mars, despite the fact that Earth is already a pretty great place to live if you're an entitled billionaire, and acquiring Spitter so he could his very own giant playground for spewing hate speech.

Despite his dubious and morally "question"able track record, such as doing drugs during a live interview on the TV show Morally "Question"able, Muzk has somehow amassed a legion of followers who worship him as a visionary and savior of humanity. Whether he is truly a genius or simply a lucky charlatan remains to be seen, but one thing is certain: he will continue to dominate headlines and attract both adoration and scorn for as long as he can keep his observable-universe-sized ego inflated and his bank account flush.

And today happens to be Ezon's lucky day. It's his birthday! But Ezon Muzk is a CEO. His schedule is way too packed to have time to celebrate frivolous nonsense like birthdays.

Instead, Ezon is trying the latest trend he read about this morning on Spitter: communicating using only emojis.

First up: a board meeting for Tezla. Ezon arrives five minutes late from spending way too much time on the toilet reading about this emoji trend, and all the executives simultaneously yell "Happy birthday!" as he enters.

"👍🥰🚫🥳🤖🚗🧑‍💼" Ezon replies.

Everyone stares at Ezon as if he is a Martian.

Finally, one executive speaks, "Um, I think Mr. Muzk wants us to start today's meeting about our current progress with autonomous driving instead of wasting time celebrating his birthday."

"👍🙂" Ezon says.

Another executive launches into a long presentation about the latest developments in Tezla's autonomous driving department.

"🤖🚗📈🧠💻💻💻💻📈" interrupts Ezon.

Everyone again stares at Ezon as if he is a Martian.

"Uhhh, we need more compute?" asks a confused executive.

"We always need more compute," the first executive says.

"💻💻💻💻💻💻💻💻💻💻💻💻📈" says Ezon.

"OK, gotcha," says the executive giving the presentation.

The same process inevitably repeats a few more times, until Ezon realizes he scheduled an overlapping meeting. "⏲️🥩🏃😥👋" he says, and dashes off to the next meeting.

Ezon finally arrives one hour late, after taking a detour for lunch at an overly upscale sushi restaurant with a one-year-long waitlist (zero-year-long waitlist if you pay a billion dollars) and mediocre sushi. This meeting is for Muzk Inc., the bogus shell company of all of Ezon's enterprises. The executives are all wearing trench coats and deep in discussion about the surprisingly difficult logistics of world domination.

"We've saturated the federal government with lobbyists, but it turns our lobbyists are less soluble than we originally thought," one executive says.

"Don't worry. Once we acquire all other tech companies in existence to achieve total tech dominance, we'll gain their lobbyists too," another executive reassures them.

"But we've hired the most soluble lobbyists in the world. Surely adding more lobbyists to the mixture will not be effective."

"Maybe if we increase the temperature of the government through climate change and then cool the world rapidly, we can achieve supersaturation."

"No, supersaturation is a metastable state. It'll easily be perturbed from equilibrium. And plus, if the temperature gets too high, it might melt the lobbyists."

"What if the government blocks all these mergers and acquisitions? Isn't it a bit suspicious to purchase 10000 companies in one day?"

"That's what lobbyists are for. Trapping the government in molasses, sometimes physically. They won't see it coming."

"🐚🧑‍💼🌎🌏🔫💰💰🇺🇸🫗💉🤣" Ezon interrupts.

"What??" one executive says, caught off-guard.

Before Ezon can respond, he realizes he actually scheduled three overlapping meetings, and dashes off again to the next meeting.

Several agents are sitting around an absurdly long table. Ezon takes a seat at the end. One of the agents at the other end begins, "We have intercepted private electronic communication between Bobert Wender and Jazelle Muzk. Here is a printout." The agent extracts a manila folder from his briefcase and throws it across the absurdly long table to Ezon.

Ezon opens up the folder and glances at the sheer density of Gen-Z slang.

"⬜❓❓" Ezon asks.

"Huh?" the agent says.

"I think Mr. Muzk is asking about the square boxes everywhere on the printout," another agent suggests.

"👍" Ezon says.

"Those are emojis. Or at least the ghosts of emojis. The software we used for making the printout predates emojis, and we've never updated it since that could break our highly optimized and refined workflow," the agent responds.

"😱😱👻🚫🙂😥🦫🇷🇪🥋🧬🌮❓❓❓ 🥶💻🦠🚫📲❓❓❓"

"I don't understand," the agent says, quickly glancing at the others to check if anyone else gets it.

"🤔🇭 🇹 🇦 🇹 🇸❓❓❓"

"I think that's a typo for that's," an agent replies.

"👍🙂❗ 🤔🤔🤔🇮 🇳 🇧 4️⃣❓❓❓"

"I give up."

### 1.111 Aydyn

I'm not evil.

I swear.

I'm not a bad person\
Even if you see me that way\
I can explain why\
I had to do it.

I was offered a deal\
In a random phone call\
I was sure it was spam\
But I've always wanted to prank a spammer.

So I answered, and the voice on the other end\
Introduced herself as Jazelle, niece of Ezon Muzk\
Hooray, definitely a spammer\
Time to have some fun!

But then she uttered the name Bobert\
My spine froze, I nearly dropped my phone\
How'd she know? How'd she know?\
But what she said next shocked me to the core.

The offer was simple, but tough\
To betray my friends, that was enough.

In return, she promised me a cool internship\
At Tezla, Ezon's electric car company\
She told me it was my future\
It was too good to pass by.

I sat staring blanking\
For hours\
After the call.

Evil!\
I thought\
But I assured myself\
This wasn't evil\
This a rational choice.

And the offer was too tempting to pass\
A future so bright, I couldn't surpass.

And with a future career at Tezla\
Making mounds of money\
Think of all the ways I could help the world.

My friends?

Just a small sacrifice for a greater good.

But I knew if I betrayed them\
I would never forget.

They would never forget.

It would always be at the back of my head\
Gnawing away.

Always there.

But Tezla!

An amazing future!\
Money!\
The pride of my parents!

A chance to work with the best\
To learn and to create\
An opportunity of a lifetime\
My future to shape.

Never having to worry about college\
Or jobs\
Or finances\
Any of that\
Just one sacrifice.

The choice I had to make\
Was it worth it in the end?

My morals weighed against a dream\
I couldn't comprehend.

My mind told me what I must do\
But my heart was stuck too.

It was real\
It was a done deal.

I called her back\
She explained the mission\
Install a keylogger on Bella's computer\
Then kidnap the Wenders\
And brainwash them into submission.

She would handle Bobert and Bella\
I would lure Billiam\
Into a devious trap\
Easy as that.

I'm a bit confused why\
This plot makes no sense\
But I have to go along with it\
It's my only choice.

My plan is to tell Bobert\
That his siblings were being kidnapped\
Once he thinks he's saved them\
Nope, he's just snared in the trap.

Jazelle was thrilled by my idea\
She loved the reverse psychology\
She said it was genius\
The kind of genius that Tezla loves.

I'm off to Tezla, feeling proud and elated\
But deep inside, I'm feeling deflated.

### 10.000 Gerrit FFFFFF

As a young boy, Gerrit FFFFFF always had a knack for math. While other kids his age dreaded the dull tedium of math class, Gerrit dreaded it even more. The endless hours of learning about fractions or some basic algebra that he had self-studied long ago... Gerrit knew that when he grew up, he wanted to become a math teacher to finally put an end once and for all to the horrors of math class by tearing down the curriculum and rebuilding it so it would actually be somewhat interesting.

In college, Gerrit majored in education and minored in mathematics, preparing for a bright future of revolutionizing math education. Unfortunately, his friends weren't so enthusiastic. "Gerrit, you got to ride the latest hype wave in San Silicono! If you join a startup now, you'll be a millionaire by the time you drop out of college!" they said excitedly, thrilled to shill their favorite tech startups. Gerrit, being an impressionable and impressed young man, knew zero programming (in fact, he knew less than zero programming, because he required more time than the average Joe to learn it), joined his friends on their career mistake and joined a startup called Gerrit, which was developing a fledgling web-based code collaboration tool also called Gerrit. Gerrit (the company) was thrilled to have someone named after the company join them, so Gerrit (the person)'s career started out with a blast! Even more fortuitously, Gerrit's last name, FFFFFF, was what all the Gerrit (the company) developers screamed when they were debugging code.

As time went on, Gerrit (the person) realized that his impulsive career choice wasn't in fact a mistake. It was a tough job. Gerrit had the opposite of a knack for programming and he had put in hundreds of hours a valiant attempt to learn it. On the other hand, Gerrit (the company) was skyrocketing! Ecstatic, he quickly dropped out of college and became a Gerrit developer full-time. His friends on the other hand weren't so lucky (especially since they weren't named Gerrit) and the startups they had joined had lost of their money and crashed and burned.

Soon, Gerrit (the tool) was having performance problems due to being written in Python (a language that Gerrit (the person) still had a lot of trouble understanding), so Gerrit (the company) held a meeting to brainstorm a solution. Gerrit (the person) suggested rewriting the entire thing in APL (a programming language), and everyone applauded the genius of the idea. Gerrit (the person) thus began the drudgery of sinking hundreds more hours trying to learn yet another programming language, APL, and Gerrit (the company) began the Gerrit (the tool) rewrite.

Eventually, Gerrit (the company) caught the eye of the gargantuan G, Googol, and the Gerrit (the company) executives began negotiating an acquisition with Google. A few weeks later, everyone celebrated the purchase and Gerrit (the company) became part of Googol, Gerrit (the tool) became a Googol product, and Gerrit (the person) became a Googoler. However, Gerrit (the person) was the only one sad about the purchase. He had made a big career mistake. He wasn't making millions. His salary had stayed the same, even with all he that had contributed to Gerrit (the company).

The next day, Gerrit (the person) announced his resignation from Gerrit (the company) and became a high school math teacher at LHWHS. The job was tough at first, but rewarding. He loved impacting the lives of so many kids and finally showing them the beauty of math. The school administration also made him teach AP Computer Science too, mainly because he was the only teacher there who actually knew what binary was.

One day, Billiam Wender, Gerrit FFFFFF's third-brightest math student, asked him about his experiences being a math teacher and Gerrit launched into the story you just read. As the story came to a close, Billiam asked curiously, "I don't get it. Why aren't you a programmer anymore?"

Gerrit FFFFFF replied, "Well, I never got arrays."

"Billiam!"

It's the end of an arduous school day at LHWHS, and Billiam is standing at Mr. FFFFFF's desk deep in conversation as the rest of the students file out of the room. Aydyn is at the doorway, trying to squeeze through the elaborately planned congestion.

"Billiam! I ran all the way across the building to tell you something," Aydyn yelps, gasping for breath.

"What?"

"Your siblings are in serious trouble."

"What now?"

"I don't know. I think they've been kidnapped."

Billiam throws up his hands and says, "Wow, just what I need. Just when I thought the universe couldn't troll me more."

"You have to come with me now!"

"Why?"

"What do you mean, why? You have to come help me save your siblings!"

"Why? What can I even do?"

"Believe in yourself, Billiam," Mr. FFFFFF interjects, spewing some generic inspiration garbage.

"You're right," Billiam agrees, as if the inspiration garbage actually works, "I can't just give up."

"Let's go!" Aydyn hurriedly says, grabbing Billiam's arm, "We don't have much time!"

The two rush out of the classroom, and start running down the hall. Billiam can't help but notice everyone staring at them.

"Aydyn," Billiam mutters, "I'm sorry."

"What's wrong?"

"I really can't do this."

"Why not?"

"Everyone's staring at me."

"Maybe because we're running like idiots in the hall. Or because of what happened at the LHWHSCSC meeting yesterday."

"Don't talk about it!" Billiam suddenly screams, "I give up! This is stupid! The universe just wants to laugh at me. And taunt me. And make a total fool out of me."

"No. Believe in yourself. You can't give up now."

"Shut up! That's just a load of inspiration bullshit."

"Come on, Billiam!"

"I can't. I give up. I'm leaving."

"Billiam! No!"

In a fit of rage and frustration, Billiam charges down the hallway and finally runs outside, watching in despair as the school bus leaves without him.

"No! This is stupid! Now this?"

He sprints after the bus, but to his surprise, the driver stops and lets him climb aboard. The bus ride is as bland and eventful as usual, reassuring Billiam that maybe some things can at least return to normal. Billiam finally arrives home and opens the door.

"Billiam! Are you OK?"

"No, I'm not," Billiam starts to say, but then he recognizes the voice, "Mr. FFFFFF? Wait what? What are you doing inside my house?"

"Billiam, listen to me. You cannot give up. Very bad things are happening right now. Your siblings need you. I need to tell you something. That's why I came to your house."

"Where are my parents?"

"They're at the police station right now?"

"Wha... why?"

"We don't have time for this. No one knows who's behind the kidnappings. Billiam, remember this: 123 Example Street. Go there and someone will help you. This is more serious than any of us expected. You have to believe in yourself."

Billiam looks up the address on his phone, but as he physically looks up, he sees a gun pointed straight at him.

"Jazelle???" Billiam utters, utterly shocked.

"Don't move!" Jazelle says menacingly, "This is no ordinary gun. This gun has the special ability that when shot at someone, it causes that person to become delusional and believe they are a cat."

"So it's anime," Billiam laughs, trying defusing the tension, "You really expect me to believe what you just said?"

"Shut up!" Jazelle yells, "Don't move a muscle."

"Please, I don't know who you are, but please don't hurt us," Mr. FFFFFF pleads, stepping between Billiam and Jazelle.

Billiam seizes the moment to sprint towards the back door. Lucky for him, he makes it out alive. Mr. FFFFFF is not so lucky.

The sound of gunshots and grotesque thoughts throb in Billiam's brain, as he runs for his life to 123 Example Street. His muscles cramp like crazy. his lungs feel like they're on fire. After the 10 most grueling minutes of his life, he finally arrives, collapsing right in front of the front door.

"Hope," he says, staring at the doormat which actually says "Home." His vision fogs up and his imagination goes haywire. The house's bricks transform into tons of Wii consoles. He sees Bella mining crypto on her machine learning rig. He watches Bobert make instant ramen using black tea instead of water. He sees Lalani and Jazelle cheerfully collaborating to fry some spring rolls using gasoline as a cooking oil substitute. "Computers are evil!" Billiam screams. Mr. FFFFFF tries eating a CPU to gain more brainpower. Fascist unicorns arrive and arrest Billiam for being an enemy of the state of Muzk. 10 copies of Ezon suddenly appear and start barking like dogs simultaneously and synchronized. The Ezons suddenly merge and face morph into the shape of an elderly woman opening the front door.

"Kids these days..."
